package pages;

public class InsertFechaCalendario_Page extends Base_Page  {

    public static String environment = "src/test/resources/insert/Insert_FechaCalendario.xml";

    /*************************
     ***** INSERT ORIGEN *****
     *************************/

    public static String insert_DIAS_FESTIVOS08() {
        String insert = fetchQuery(environment).getProperty("insert_DIAS_FESTIVOS08");
        return insert;
    }

    public static String insert_DIAS_FESTIVOS09() {
        String insert = fetchQuery(environment).getProperty("insert_DIAS_FESTIVOS09");
        return insert;
    }

    public static String insert_DIAS_FESTIVOS10() {
        String insert = fetchQuery(environment).getProperty("insert_DIAS_FESTIVOS10");
        return insert;
    }

    /**********************
     ***** INSERT MCP *****
     **********************/

    public static String insertPROCESO_METRICA_REL_N1() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N1");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N1() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N1");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N2() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N2");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N2() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N2");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N3() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N3");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N3() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N3");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N4() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N4");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N4() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N4");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N5() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N5");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N5() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N5");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N6() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N6");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N6() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N6");
        return insert;
    }
}
