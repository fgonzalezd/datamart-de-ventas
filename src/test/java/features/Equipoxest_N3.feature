@equipoxest_N3

Feature: Equipoxest N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para Equipoxest "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Equipoxest caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para Equipoxest caso N_tres
    And se debe insertar el dataset para las tablas de hive para Equipoxest caso N_tres
    Then se debe ejecutar el proceso de extraccion para Equipoxest caso N_tres

  Scenario: Validacion para tabla Equipoxest "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla Equipoxest caso N_tres
    And hago una consulta a la tabla del MCP para tabla Equipoxest caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla Equipoxest caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Equipoxest caso N_tres

  Scenario: Validacion de archivos en hdfs para Equipoxest "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los archivos en TO_IMPORT para Equipoxest caso N_tres
    And hago una consulta a los archivos en TO_TRANSFER para Equipoxest caso N_tres
    And hago una consulta a los archivos en LOADING para Equipoxest caso N_tres
    Then valido que la cantidad de archivos en el hdfs sea correcta para Equipoxest caso N_tres

  Scenario: Validacion de logs Equipoxest "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Equipoxest caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Equipoxest caso N_tres

  Scenario: Limpieza ambientacion para Equipoxest "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Equipoxest caso N_tres