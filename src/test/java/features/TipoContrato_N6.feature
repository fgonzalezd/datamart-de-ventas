@tipoContrato_N6

Feature: Tipo_Contrato N6 - Metrica activa con regla OK

  Scenario: Ambientacion para Tipo_Contrato "Metrica activa con regla OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Tipo_Contrato caso N_Seis
    When se debe insertar el dataset para las tablas de Oracle para Tipo_Contrato caso N_Seis
    And se debe insertar el dataset para las tablas de hive para Tipo_Contrato caso N_Seis
    Then se debe ejecutar el proceso de extraccion para Tipo_Contrato caso N_Seis

  Scenario: Validacion para tabla Tipo_Contrato "Metrica activa con regla OK"
    When hago una consulta a la tabla del RAW para tabla Tipo_Contrato caso N_Seis
    And hago una consulta a la tabla del MCP para tabla Tipo_Contrato caso N_Seis
    Then valido que la insercion a la tabla RAW sea correcta para tabla Tipo_Contrato caso N_Seis
    And valido que el registro de insercion en el MCP fue correcto para tabla Tipo_Contrato caso N_Seis

  Scenario: Validacion de archivos en hdfs para Tipo_Contrato "Metrica activa con regla OK"
    When hago una consulta a los archivos en TO_IMPORT para Tipo_Contrato caso N_Seis
    And hago una consulta a los archivos en TO_TRANSFER para Tipo_Contrato caso N_Seis
    And hago una consulta a los archivos en LOADING para Tipo_Contrato caso N_Seis
    Then valido que la cantidad de archivos en el hdfs sea correcta para Tipo_Contrato caso N_Seis

  Scenario: Validacion de logs Tipo_Contrato "Metrica activa con regla OK"
    When hago una consulta a los logs generados en RAW para Tipo_Contrato caso N_Seis
    Then valido que la cantidad de logs generados sea correcto para Tipo_Contrato caso N_Seis

  Scenario: Limpieza ambientacion para Tipo_Contrato "Metrica activa con regla OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Tipo_Contrato caso N_Seis