@parcat_Debito_N5

Feature: parcat_Debito N5 - Metrica activa sin regla

  Scenario: Ambientacion para "Metrica activa sin regla"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para parcat_Debito caso N_cinco
    When se debe insertar el dataset para las tablas de Oracle para parcat_Debito caso N_cinco
    And se debe insertar el dataset para las tablas de hive para parcat_Debito caso N_cinco
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para parcat_Debito caso N_cinco

  Scenario: Validacion para tabla parcat_Debito "Metrica activa sin regla"
    When hago una consulta a la tabla del RAW para tabla parcat_Debito caso N_cinco
    And hago una consulta a la tabla del MCP para tabla parcat_Debito caso N_cinco
    Then valido que la insercion a la tabla RAW sea correcta para tabla parcat_Debito caso N_cinco
    And valido que el registro de insercion en el MCP fue correcto para tabla parcat_Debito caso N_cinco

  Scenario: Limpieza ambientacion para "Metrica activa sin regla"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para parcat_Debito caso N_cinco