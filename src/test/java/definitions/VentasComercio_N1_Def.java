package definitions;

import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentTxs_Page;
import pages.InsertVentasComercio_Page;
import pages.QueryTableTxs_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class VentasComercio_N1_Def {

    public static PropertiesInit prop;
    public static InsertVentasComercio_Page page;
    public static QueryTableTxs_Page query;
    public static EnvironmentTxs_Page environment;
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvLOB = "src/test/resources/dataset/VentasComercio/ventasComercio_N1.csv";
    public ArrayList<String[]> destinoLOB = new ArrayList<>();
    public ArrayList<String[]> datasetLOB = new ArrayList<>();

    @When("^se debe limpiar el ambiente para la ejecucion de pruebas de Ventas_Comercios caso N_uno$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_de_Ventas_Comercios_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), environment.deleteTX_PROCESADAS());
    }

    @Then("^se debe insertar el dataset para las tablas de CUR de Ventas_Comercios caso N_uno$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_CUR_de_Ventas_Comercios_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), page.insert_TX_PROCESADAS01());
    }

    @When("^hago una consulta a la tabla LOB para vista Ventas_Comercios caso N_uno$")
    public void hago_una_consulta_a_la_tabla_LOB_para_vista_Ventas_Comercios_caso_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserLOB(), prop.PassLOB());
            connHive.setSchema(prop.HiveSchemaCurTxProcesadas());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryVENTAS_COMERCIO());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoLOB.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvLOB))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetLOB.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido la visualizacion sea correcta para vista Ventas_Comercios caso N_uno$")
    public void valido_la_visualizacion_sea_correcta_para_vista_Ventas_Comercios_caso_N_uno() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryVENTAS_COMERCIO_COUNT());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoLOB.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(datasetLOB.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino CUR " + i + " = " + Arrays.deepToString(destinoLOB.get(i)));
            System.out.println("Dataset CUR " + i + " = " + Arrays.deepToString(datasetLOB.get(i)));
//            assertTrue(Arrays.deepToString(destinoRAW.get(i)).equals(Arrays.deepToString(destinoRAW.get(i))));
        }
        System.out.println(" ");
        destinoLOB.clear();
        datasetLOB.clear();
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Ventas_Comercios caso N_uno$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Ventas_Comercios_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), environment.deleteTX_PROCESADAS());
    }
}