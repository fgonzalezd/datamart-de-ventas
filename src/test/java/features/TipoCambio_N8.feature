@tipoCambio_N8

Feature: Tipo_Cambio N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Tipo_Cambio caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Tipo_Cambio caso N_Ocho
    Then se debe ejecutar el proceso de extraccion para Tipo_Cambio caso N_Ocho

  Scenario: Validacion de DRP Tipo_Cambio "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum de los archivos de origen Tipo_Cambio caso N_Ocho
    And hago una consulta al checksum de los archivos transferidos Tipo_Cambio caso N_Ocho
    Then valido que los archivos transferidos esten correcto Tipo_Cambio caso N_Ocho

  Scenario: Validacion de logs Tipo_Cambio "Transferencia de multiples archivos DRP"
    When hago una consulta a los logs generados en RAW para Tipo_Cambio caso N_Ocho
    Then valido que la cantidad de logs generados sea correcto para Tipo_Cambio caso N_Ocho

  Scenario: Limpieza ambientacion para "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Tipo_Cambio caso N_Ocho