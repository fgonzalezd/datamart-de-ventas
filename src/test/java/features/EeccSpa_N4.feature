@eeccSpa_N4

Feature: EECC_SPA N4 - Metrica deshabilitada

  Scenario: Ambientacion para EECC_SPA "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para EECC_SPA caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para EECC_SPA caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para EECC_SPA caso N_cuatro
    Then se debe ejecutar el proceso de extraccion para EECC_SPA caso N_cuatro

  Scenario: Validacion para tabla EECC_SPA "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla EECC_SPA caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla EECC_SPA caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla EECC_SPA caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla EECC_SPA caso N_cuatro

  Scenario: Validacion de archivos en hdfs para EECC_SPA "Metrica deshabilitada"
    When hago una consulta a los archivos en TO_IMPORT para EECC_SPA caso N_cuatro
    And hago una consulta a los archivos en TO_TRANSFER para EECC_SPA caso N_cuatro
    And hago una consulta a los archivos en LOADING para EECC_SPA caso N_cuatro
    Then valido que la cantidad de archivos en el hdfs sea correcta para EECC_SPA caso N_cuatro

  Scenario: Validacion de logs EECC_SPA "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para EECC_SPA caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para EECC_SPA caso N_cuatro

  Scenario: Limpieza ambientacion para EECC_SPA "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para EECC_SPA caso N_cuatro