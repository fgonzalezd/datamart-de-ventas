@ctssServicios_N3

Feature: Ctss_Servicios N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para Ctss_Servicios "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Ctss_Servicios caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para Ctss_Servicios caso N_tres
    And se debe insertar el dataset para las tablas de hive para Ctss_Servicios caso N_tres
    Then se debe ejecutar el proceso de extraccion para Ctss_Servicios caso N_tres

  Scenario: Validacion para tabla Ctss_Servicios "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla Ctss_Servicios caso N_tres
    And hago una consulta a la tabla del MCP para tabla Ctss_Servicios caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla Ctss_Servicios caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Ctss_Servicios caso N_tres

  Scenario: Validacion de archivos en hdfs para Ctss_Servicios "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los archivos en TO_IMPORT para Ctss_Servicios caso N_tres
    And hago una consulta a los archivos en TO_TRANSFER para Ctss_Servicios caso N_tres
    And hago una consulta a los archivos en LOADING para Ctss_Servicios caso N_tres
    Then valido que la cantidad de archivos en el hdfs sea correcta para Ctss_Servicios caso N_tres

  Scenario: Validacion de logs Ctss_Servicios "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Ctss_Servicios caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Ctss_Servicios caso N_tres

  Scenario: Limpieza ambientacion para Ctss_Servicios "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Ctss_Servicios caso N_tres