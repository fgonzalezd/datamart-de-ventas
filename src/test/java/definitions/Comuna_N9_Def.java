package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentMstrgeo_Page;
import pages.InsertComuna_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class Comuna_N9_Def {

    public static PropertiesInit prop;
    public static EnvironmentMstrgeo_Page environment;
    public static InsertComuna_Page page;
    public ArrayList<String> logs = new ArrayList<>();

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Comuna caso N_Nueve$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Comuna_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.logCOMUNA());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsCOMUNA());
    }

    @When("^Se ejecuta el proceso de extraccion con una fecha erronea para Comuna caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_extraccion_con_una_fecha_erronea_para_Comuna_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 62 0190709 MSTRGEO N");
    }

    @Then("^se valida que el proceso de extraccion se haya terminado con error Comuna caso N_Nueve$")
    public void se_valida_que_el_proceso_de_extraccion_se_haya_terminado_con_error_Comuna_caso_N_Nueve() throws Throwable {
        
    }

    @When("^Se ejecuta el proceso de carga STG con una fecha erronea para Comuna caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_STG_con_una_fecha_erronea_para_Comuna_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 63 S01907j9 MSTRGEO N");
    }

    @Then("^se valida que el proceso de carga STG se haya terminado con error Comuna caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_STG_se_haya_terminado_con_error_Comuna_caso_N_Nueve() throws Throwable {
        
    }

    @When("^Se ejecuta el proceso de carga RAW con una fecha erronea para Comuna caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_RAW_con_una_fecha_erronea_para_Comuna_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 65 2019:07:09 MSTRGEO N");
    }

    @Then("^se valida que el proceso de carga RAW se haya terminado con error Comuna caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_RAW_se_haya_terminado_con_error_Comuna_caso_N_Nueve() throws Throwable {
        
    }

    @When("^hago una consulta a los logs generados en RAW para Comuna caso N_Nueve$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Comuna_caso_N_Nueve() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/mstrgeo/comuna -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String find = "";
            while ((find = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + find);
                logs.add(find);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Comuna caso N_Nueve$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Comuna_caso_N_Nueve() throws Throwable {
        assertEquals(logs, logs);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logs + " de 3");
    }
}
