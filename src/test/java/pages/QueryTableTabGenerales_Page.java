package pages;

public class QueryTableTabGenerales_Page extends Base_Page {

    public static String queryXML = "src/test/resources/query/QueryTable_TabGenerales.xml";

    /**********************
     ***** QUERY HIVE *****
     **********************/

    public static String queryDIAS_FESTIVOS() {
        String query = fetchQuery(queryXML).getProperty("queryDIAS_FESTIVOS");
        return query;
    }

    public static String queryDIAS_FESTIVOS_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryDIAS_FESTIVOS_COUNT");
        return query;
    }

    public static String queryTIPO_CAMBIO() {
        String query = fetchQuery(queryXML).getProperty("queryTIPO_CAMBIO");
        return query;
    }

    public static String queryTIPO_CAMBIO_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryTIPO_CAMBIO_COUNT");
        return query;
    }

    public static String queryFECHA_CALENDARIO() {
        String query = fetchQuery(queryXML).getProperty("queryFECHA_CALENDARIO");
        return query;
    }

    public static String queryFECHA_CALENDARIO_FESTIVO() {
        String query = fetchQuery(queryXML).getProperty("queryFECHA_CALENDARIO_FESTIVO");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_DIAS_FESTIVOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_DIAS_FESTIVOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_DIAS_FESTIVOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_DIAS_FESTIVOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_TIPO_CAMBIO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_TIPO_CAMBIO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_TIPO_CAMBIO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_TIPO_CAMBIO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_FECHA_CALENDARIO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_FECHA_CALENDARIO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_FECHA_CALENDARIO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_FECHA_CALENDARIO");
        return query;
    }
}
