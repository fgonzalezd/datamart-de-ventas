package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.*;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class TxProcesadas_N2_Def {

    public static PropertiesInit prop;
    public static InsertTxProcesadas_Page page;
    public static QueryTableTxs_Page query;
    public static EnvironmentTxs_Page environment;
    public static EnvironmentTabGenerales_Page envDias;
    public static EnvironmentParcom_Page envCom;
    public static EnvironmentArc_Page envCont;
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvCUR = "src/test/resources/dataset/TxProcesadas/txProcesadasCUR_N2.csv";
    public static String csvMCP = "src/test/resources/dataset/TxProcesadas/txProcesadasMCP_N2.csv";
    public ArrayList<String[]> destinoCUR = new ArrayList<>();
    public ArrayList<String[]> datasetCUR = new ArrayList<>();
    public ArrayList<String[]> destinoMCP = new ArrayList<>();
    public ArrayList<String[]> datasetMCP = new ArrayList<>();
    public String logs;

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Transacciones_Procesadas caso N_dos$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawParcom(), envCom.deleteCOMISION_RETENCION());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawArc(), envCont.deleteCONTRATOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), environment.deleteTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTiempo(), envDias.deleteFECHA_CALENDARIO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), environment.deleteTX_PROCESADAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserCUR(), prop.PassCUR(), "rm -R /data/environment/hdp_log/load_cur/tx_proc/transaccion_procesada");
    }

    @When("^se debe insertar el dataset para las tablas de RAW para Transacciones_Procesadas caso N_dos$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_RAW_para_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawParcom(), page.insert_COMISION_RETENCION08());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawArc(), page.insert_CONTRATOS08());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), page.insert_MONETARIAS08());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTiempo(), page.insert_FECHA_CALENDARIO());
    }

    @When("^se debe insertar el dataset para las tablas de CUR para Transacciones_Procesadas caso N_dos$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_CUR_para_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N2());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO_N2());
    }

    @Then("^se debe ejecutar el proceso de extraccion con una fecha del (\\d+)/(\\d+)/(\\d+) para Transacciones_Procesadas caso N_dos$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_con_una_fecha_del_para_Transacciones_Procesadas_caso_N_dos(int arg1, int arg2, int arg3) throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserCUR(), prop.PassCUR(),
                "sh -x /data/environment/hdp_app/load_cur/tx_proc/shl/Shl_carga_cur_transaccion_procesada.sh 29 20190708 TX_PROC N");
    }

    @When("^hago una consulta a la tabla del CUR para tabla Transacciones_Procesadas caso N_dos$")
    public void hago_una_consulta_a_la_tabla_del_CUR_para_tabla_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserCUR(), prop.PassCUR());
            connHive.setSchema(prop.HiveSchemaCurTxProcesadas());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryTX_PROCESADAS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoCUR.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvCUR))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetCUR.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("^hago una consulta a la tabla del MCP para tabla Transacciones_Procesadas caso N_dos$")
    public void hago_una_consulta_a_la_tabla_del_MCP_para_tabla_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserCUR(), prop.PassCUR());
            connHive.setSchema(prop.HiveSchemaMCP());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_TX_PROCESADAS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoMCP.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvMCP))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetMCP.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido que la insercion a la tabla CUR sea correcta para tabla Transacciones_Procesadas caso N_dos$")
    public void valido_que_la_insercion_a_la_tabla_CUR_sea_correcta_para_tabla_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryTX_PROCESADAS_COUNT());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoCUR.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoCUR.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino CUR " + i + " = " + Arrays.deepToString(destinoCUR.get(i)));
            System.out.println("Dataset CUR " + i + " = " + Arrays.deepToString(datasetCUR.get(i)));
//            assertTrue(Arrays.deepToString(destinoRAW.get(i)).equals(Arrays.deepToString(destinoRAW.get(i))));
        }
        System.out.println(" ");
        destinoCUR.clear();
        datasetCUR.clear();
    }

    @Then("^valido que el registro de insercion en el MCP fue correcto para tabla Transacciones_Procesadas caso N_dos$")
    public void valido_que_el_registro_de_insercion_en_el_MCP_fue_correcto_para_tabla_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_COUNT_TX_PROCESADAS());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            System.out.println("Dataset MCP " + i + " = " + Arrays.deepToString(datasetMCP.get(i)));
//            assertTrue(Arrays.deepToString(destinoMCP.get(i)).equals(Arrays.deepToString(destinoMCP.get(i))));
        }
        System.out.println(" ");
        destinoMCP.clear();
        datasetMCP.clear();
    }

    @When("^hago una consulta a los logs generados en RAW para Transacciones_Procesadas caso N_dos$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.UserCUR(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.PassCUR());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("find /data/environment/hdp_log/load_cur/tx_proc/transaccion_procesada -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((logs = reader.readLine()) != null) {
                System.out.println(++index + " : " + logs);
                Reporter.addStepLog(logs);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Transacciones_Procesadas caso N_dos$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Transacciones_Procesadas_caso_N_dos() throws Throwable {
//        assertEquals("1", logs);
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Transacciones_Procesadas caso N_dos$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Transacciones_Procesadas_caso_N_dos() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawParcom(), envCom.deleteCOMISION_RETENCION());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawArc(), envCont.deleteCONTRATOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), environment.deleteTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTiempo(), envDias.deleteFECHA_CALENDARIO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), environment.deleteTX_PROCESADAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
    }
}
