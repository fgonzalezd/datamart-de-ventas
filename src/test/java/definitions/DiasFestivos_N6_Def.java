package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentTabGenerales_Page;
import pages.InsertDiasFestivos_Page;
import pages.QueryTableTabGenerales_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class DiasFestivos_N6_Def {

    public static PropertiesInit prop;
    public static EnvironmentTabGenerales_Page environment;
    public static InsertDiasFestivos_Page page;
    public static QueryTableTabGenerales_Page query;
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvRAW = "src/test/resources/dataset/DiasFestivos/diasFestivosRAW_N6.csv";
    public static String csvMCP = "src/test/resources/dataset/DiasFestivos/diasFestivosMCP_N6.csv";
    public ArrayList<String[]> destinoRAW = new ArrayList<>();
    public ArrayList<String[]> datasetRAW = new ArrayList<>();
    public ArrayList<String[]> destinoMCP = new ArrayList<>();
    public ArrayList<String[]> datasetMCP = new ArrayList<>();
    public String logs;

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para dias_Festivos caso N_Seis$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_dias_Festivos_caso_N_Seis() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropDIAS_FESTIVOS());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createDIAS_FESTIVOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTab(), environment.deleteDIAS_FESTIVOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), "rm -R /data/environment/hdp_log/load_raw/tabgenerales/dias_festivos");
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para dias_Festivos caso N_Seis$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_dias_Festivos_caso_N_Seis() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN());
    }

    @When("^se debe insertar el dataset para las tablas de hive para dias_Festivos caso N_Seis$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_hive_para_dias_Festivos_caso_N_Seis() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTab(), page.insert_RAW1());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTab(), page.insert_RAW2());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N6());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO1_N6());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO2_N6());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO3_N6());
    }

    @Then("^se debe ejecutar el proceso de extraccion con una fecha del (\\d+)/(\\d+)/(\\d+) para dias_Festivos caso N_Seis$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_con_una_fecha_del_para_dias_Festivos_caso_N_Seis(int arg1, int arg2, int arg3) throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 40 20190710 TABGENERALES N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 41 20190710 TABGENERALES N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 43 20190710 TABGENERALES N");
    }

    @When("^hago una consulta a la tabla del RAW para tabla dias_Festivos caso N_Seis$")
    public void hago_una_consulta_a_la_tabla_del_RAW_para_tabla_dias_Festivos_caso_N_Seis() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaRawTab());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryDIAS_FESTIVOS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoRAW.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvRAW))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetRAW.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("^hago una consulta a la tabla del MCP para tabla dias_Festivos caso N_Seis$")
    public void hago_una_consulta_a_la_tabla_del_MCP_para_tabla_dias_Festivos_caso_N_Seis() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaMCP());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_DIAS_FESTIVOS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoMCP.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvMCP))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetMCP.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido que la insercion a la tabla RAW sea correcta para tabla dias_Festivos caso N_Seis$")
    public void valido_que_la_insercion_a_la_tabla_RAW_sea_correcta_para_tabla_dias_Festivos_caso_N_Seis() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryDIAS_FESTIVOS_COUNT());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino RAW " + i + " = " + Arrays.deepToString(destinoRAW.get(i)));
            System.out.println("Dataset RAW " + i + " = " + Arrays.deepToString(datasetRAW.get(i)));
//            assertTrue(Arrays.deepToString(destinoRAW.get(i)).equals(Arrays.deepToString(destinoRAW.get(i))));
        }
        System.out.println(" ");
        destinoRAW.clear();
        datasetRAW.clear();
    }

    @Then("^valido que el registro de insercion en el MCP fue correcto para tabla dias_Festivos caso N_Seis$")
    public void valido_que_el_registro_de_insercion_en_el_MCP_fue_correcto_para_tabla_dias_Festivos_caso_N_Seis() throws Throwable {
//Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_COUNT_DIAS_FESTIVOS());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            System.out.println("Dataset MCP " + i + " = " + Arrays.deepToString(datasetMCP.get(i)));
//            assertTrue(Arrays.deepToString(destinoMCP.get(i)).equals(Arrays.deepToString(destinoMCP.get(i))));
        }
        System.out.println(" ");
        destinoMCP.clear();
        datasetMCP.clear();
    }

    @When("^hago una consulta a los logs generados en RAW para dias_Festivos caso N_Seis$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_dias_Festivos_caso_N_Seis() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/tabgenerales/dias_festivos -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((logs = reader.readLine()) != null) {
                System.out.println(++index + " : " + logs);
                Reporter.addStepLog(logs);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para dias_Festivos caso N_Seis$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_dias_Festivos_caso_N_Seis() throws Throwable {
//        assertEquals("3", logs);
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para dias_Festivos caso N_Seis$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_dias_Festivos_caso_N_Seis() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropDIAS_FESTIVOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTab(), environment.deleteDIAS_FESTIVOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
    }
}
