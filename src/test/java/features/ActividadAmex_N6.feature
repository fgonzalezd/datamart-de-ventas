@actividadAmex_N6

Feature: Actividad_Amex N6 - Metrica activa con regla OK

  Scenario: Ambientacion para "Metrica activa con regla OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Actividad_Amex caso N_Seis
    When se debe insertar el dataset para las tablas de Oracle para Actividad_Amex caso N_Seis
    And se debe insertar el dataset para las tablas de hive para Actividad_Amex caso N_Seis
    Then se debe ejecutar el proceso de extraccion con una fecha del 10/7/2019 para Actividad_Amex caso N_Seis

  Scenario: Validacion para tabla Actividad_Amex "Metrica activa con regla OK"
    When hago una consulta a la tabla del RAW para tabla Actividad_Amex caso N_Seis
    And hago una consulta a la tabla del MCP para tabla Actividad_Amex caso N_Seis
    Then valido que la insercion a la tabla RAW sea correcta para tabla Actividad_Amex caso N_Seis
    And valido que el registro de insercion en el MCP fue correcto para tabla Actividad_Amex caso N_Seis

  Scenario: Limpieza ambientacion para "Metrica activa con regla OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Actividad_Amex caso N_Seis