@tipoRubros_N1

Feature: Tipo_Rubros N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para TIPO_RUBRO caso N_uno
    When se debe insertar el dataset para las tablas de Oracle para TIPO_RUBRO caso N_uno
    And se debe insertar el dataset para las tablas de hive para TIPO_RUBRO caso N_uno
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para TIPO_RUBRO caso N_uno

  Scenario: Validacion para tabla Tipo_Rubro "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del RAW para tabla TIPO_RUBRO caso N_uno
    And hago una consulta a la tabla del MCP para tabla TIPO_RUBRO caso N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla TIPO_RUBRO caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla TIPO_RUBRO caso N_uno

  Scenario: Limpieza ambientacion para "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para TIPO_RUBRO caso N_uno