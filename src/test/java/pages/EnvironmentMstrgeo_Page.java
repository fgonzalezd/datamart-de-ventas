package pages;

public class EnvironmentMstrgeo_Page extends Base_Page{

    public static String environment = "src/test/resources/query/Environment_Mstrgeo.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createCOMUNA() {
        String create = fetchQuery(environment).getProperty("createCOMUNA");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropCOMUNA() {
        String create = fetchQuery(environment).getProperty("dropCOMUNA");
        return create;
    }


    /**********************
     ***** DELETE RAW *****
     **********************/

    public static String deleteCOMUNA() {
        String create = fetchQuery(environment).getProperty("deleteCOMUNA");
        return create;
    }


    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL_COMUNA() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_COMUNA");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }

    /**********************
     ***** DELETE LOG *****
     **********************/

    public static String logCOMUNA() {
        String create = fetchQuery(environment).getProperty("logCOMUNA");
        return create;
    }

    /***********************
     ***** DELETE HDFS *****
     ***********************/

    public static String hdfsCOMUNA() {
        String create = fetchQuery(environment).getProperty("hdfsCOMUNA");
        return create;
    }
}
