@diasFestivos_N7

Feature: dias_Festivos N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para dias_Festivos caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para dias_Festivos caso N_Siete
    Then se debe ejecutar el proceso de extraccion con una fecha del 10/7/2019 para dias_Festivos caso N_Siete

  Scenario: Validacion de DRP dias_Festivos "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del archivo origen dias_Festivos caso N_Siete
    And hago una consulta al checksum del archivo transferido dias_Festivos caso N_Siete
    Then valido que el archivo transferido este correcto dias_Festivos caso N_Siete

  Scenario: Validacion de logs dias_Festivos "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados en RAW para dias_Festivos caso N_Siete
    Then valido que la cantidad de logs generados sea correcto para dias_Festivos caso N_Siete

  Scenario: Limpieza ambientacion para "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para dias_Festivos caso N_Siete