@eeccSpa_N9

Feature: EECC_SPA N9 - Ingreso de fecha erronea en el parametro

  Scenario: Ejecucion del proceso extraccion EECC_SPA "Ingreso de fecha erronea en el parametro"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para EECC_SPA caso N_Nueve
    When Se ejecuta el proceso de extraccion con una fecha erronea para EECC_SPA caso N_Nueve
    Then se valida que el proceso de extraccion se haya terminado con error EECC_SPA caso N_Nueve

  Scenario: Ejecucion del proceso STG EECC_SPA "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga STG con una fecha erronea para EECC_SPA caso N_Nueve
    Then se valida que el proceso de carga STG se haya terminado con error EECC_SPA caso N_Nueve

  Scenario: Ejecucion del proceso RAW EECC_SPA "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga RAW con una fecha erronea para EECC_SPA caso N_Nueve
    Then se valida que el proceso de carga RAW se haya terminado con error EECC_SPA caso N_Nueve

  Scenario: Validacion de logs EECC_SPA "Ingreso de fecha erronea en el parametro"
    When hago una consulta a los logs generados en RAW para EECC_SPA caso N_Nueve
    Then valido que la cantidad de logs generados sea correcto para EECC_SPA caso N_Nueve