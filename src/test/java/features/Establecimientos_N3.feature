@establecimientos_N3

Feature: Establecimientos N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para Establecimientos "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Establecimientos caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para Establecimientos caso N_tres
    And se debe insertar el dataset para las tablas de hive para Establecimientos caso N_tres
    Then se debe ejecutar el proceso de extraccion para Establecimientos caso N_tres

  Scenario: Validacion para tabla Establecimientos "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla Establecimientos caso N_tres
    And hago una consulta a la tabla del MCP para tabla Establecimientos caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla Establecimientos caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Establecimientos caso N_tres

  Scenario: Validacion de archivos en hdfs para Establecimientos "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los archivos en TO_IMPORT para Establecimientos caso N_tres
    And hago una consulta a los archivos en TO_TRANSFER para Establecimientos caso N_tres
    And hago una consulta a los archivos en LOADING para Establecimientos caso N_tres
    Then valido que la cantidad de archivos en el hdfs sea correcta para Establecimientos caso N_tres

  Scenario: Validacion de logs Establecimientos "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Establecimientos caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Establecimientos caso N_tres

  Scenario: Limpieza ambientacion para Establecimientos "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Establecimientos caso N_tres