@cobroComision_N7

Feature: Cobro_Comision_Retencion N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Cobro_Comision_Retencion caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para Cobro_Comision_Retencion caso N_Siete
    Then se debe ejecutar el proceso de extraccion con una fecha del 10/7/2019 para Cobro_Comision_Retencion caso N_Siete

  Scenario: Validacion de DRP Cobro_Comision_Retencion "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del archivo origen Cobro_Comision_Retencion caso N_Siete
    And hago una consulta al checksum del archivo transferido Cobro_Comision_Retencion caso N_Siete
    Then valido que el archivo transferido este correcto Cobro_Comision_Retencion caso N_Siete

  Scenario: Validacion de logs Cobro_Comision_Retencion "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados en RAW para Cobro_Comision_Retencion caso N_Siete
    Then valido que la cantidad de logs generados sea correcto para Cobro_Comision_Retencion caso N_Siete

  Scenario: Limpieza ambientacion para "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Cobro_Comision_Retencion caso N_Siete