package pages;

public class QueryTableParcom_Page extends Base_Page {

    public static String queryXML = "src/test/resources/query/QueryTable_Parcom.xml";

    /*********************
     ***** QUERY RAW *****
     *********************/

    public static String queryPAR_CAT_TARIFARIA_CREDITO() {
        String query = fetchQuery(queryXML).getProperty("queryPAR_CAT_TARIFARIA_CREDITO");
        return query;
    }

    public static String queryPAR_CAT_TARIFARIA_CREDITO_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryPAR_CAT_TARIFARIA_CREDITO_COUNT");
        return query;
    }

    public static String queryPAR_CAT_TARIFARIA_DEBITO() {
        String query = fetchQuery(queryXML).getProperty("queryPAR_CAT_TARIFARIA_DEBITO");
        return query;
    }

    public static String queryPAR_CAT_TARIFARIA_DEBITO_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryPAR_CAT_TARIFARIA_DEBITO_COUNT");
        return query;
    }

    public static String queryPAR_CAT_SECTORIAL() {
        String query = fetchQuery(queryXML).getProperty("queryPAR_CAT_SECTORIAL");
        return query;
    }

    public static String queryPAR_CAT_SECTORIAL_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryPAR_CAT_SECTORIAL_COUNT");
        return query;
    }

    public static String queryCOMISION_RETENCION() {
        String query = fetchQuery(queryXML).getProperty("queryCOMISION_RETENCION");
        return query;
    }

    public static String queryCOMISION_RETENCION_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryCOMISION_RETENCION_COUNT");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_PAR_CAT_TARIFARIA_CREDITO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_PAR_CAT_TARIFARIA_CREDITO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_PAR_CAT_TARIFARIA_CREDITO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_PAR_CAT_TARIFARIA_CREDITO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_PAR_CAT_TARIFARIA_DEBITO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_PAR_CAT_TARIFARIA_DEBITO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_PAR_CAT_TARIFARIA_DEBITO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_PAR_CAT_TARIFARIA_DEBITO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_PAR_CAT_SECTORIAL() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_PAR_CAT_SECTORIAL");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_PAR_CAT_SECTORIAL() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_PAR_CAT_SECTORIAL");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COMISION_RETENCION() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COMISION_RETENCION");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_COMISION_RETENCION() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_COMISION_RETENCION");
        return query;
    }
}
