package pages;

public class EnvironmentTxs_Page extends Base_Page {

    public static String environment = "src/test/resources/query/Environment_Txs.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createTX_MONETARIAS() {
        String create = fetchQuery(environment).getProperty("createTX_MONETARIAS");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropTX_MONETARIAS() {
        String create = fetchQuery(environment).getProperty("dropTX_MONETARIAS");
        return create;
    }

    /***********************
     ***** DELETE HIVE *****
     ***********************/

    public static String deleteTX_MONETARIAS() {
        String create = fetchQuery(environment).getProperty("deleteTX_MONETARIAS");
        return create;
    }

    public static String deleteTX_PROCESADAS() {
        String create = fetchQuery(environment).getProperty("deleteTX_PROCESADAS");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }
}
