package pages;

public class EnvironmentParcom_Page extends Base_Page {

    public static String environment = "src/test/resources/query/Environment_Parcom.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createPAR_CAT_TARIFARIA_CREDITO() {
        String create = fetchQuery(environment).getProperty("createPAR_CAT_TARIFARIA_CREDITO");
        return create;
    }

    public static String createPAR_CAT_TARIFARIA_DEBITO() {
        String create = fetchQuery(environment).getProperty("createPAR_CAT_TARIFARIA_DEBITO");
        return create;
    }

    public static String createPAR_CAT_SECTORIAL() {
        String create = fetchQuery(environment).getProperty("createPAR_CAT_SECTORIAL");
        return create;
    }

    public static String createCOMISION_RETENCION() {
        String create = fetchQuery(environment).getProperty("createCOMISION_RETENCION");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropPAR_CAT_TARIFARIA_CREDITO() {
        String create = fetchQuery(environment).getProperty("dropPAR_CAT_TARIFARIA_CREDITO");
        return create;
    }

    public static String dropPAR_CAT_TARIFARIA_DEBITO() {
        String create = fetchQuery(environment).getProperty("dropPAR_CAT_TARIFARIA_DEBITO");
        return create;
    }

    public static String dropPAR_CAT_SECTORIAL() {
        String create = fetchQuery(environment).getProperty("dropPAR_CAT_SECTORIAL");
        return create;
    }

    public static String dropCOMISION_RETENCION() {
        String create = fetchQuery(environment).getProperty("dropCOMISION_RETENCION");
        return create;
    }

    /**********************
     ***** DELETE RAW *****
     **********************/

    public static String deletePAR_CAT_TARIFARIA_CREDITO() {
        String create = fetchQuery(environment).getProperty("deletePAR_CAT_TARIFARIA_CREDITO");
        return create;
    }

    public static String deletePAR_CAT_TARIFARIA_DEBITO() {
        String create = fetchQuery(environment).getProperty("deletePAR_CAT_TARIFARIA_DEBITO");
        return create;
    }

    public static String deletePAR_CAT_SECTORIAL() {
        String create = fetchQuery(environment).getProperty("deletePAR_CAT_SECTORIAL");
        return create;
    }

    public static String deleteCOMISION_RETENCION() {
        String create = fetchQuery(environment).getProperty("deleteCOMISION_RETENCION");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }
}
