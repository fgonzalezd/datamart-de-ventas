@txMonetarias_CargaHistorica_N3

Feature: tx_Monetarias Carga Historica - Reprocesar un archivo, ya procesado

  Scenario: Ambientacion para tx_Monetarias Carga Historica "Reprocesar un archivo, ya procesado"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias Carga Historica N_tres
    When transfiero el archivo a una carpeta del servidor para tx_Monetarias Carga Historica N_tres
    And muevo el archivo a una ruta hdfs para tx_Monetarias Carga Historica N_tres
    And consulto el checksum del archivo transferido para tx_Monetarias Carga Historica N_tres
    Then valido el checksum de ambos archivos para tx_Monetarias Carga Historica N_tres
    And se ejecuta el proceso para tx_Monetarias Carga Historica N_tres

  Scenario: Validacion para tabla tx_Monetarias Carga Historica "Reprocesar un archivo, ya procesado"
    When hago una consulta a la tabla del RAW para tabla tx_Monetarias Carga Historica N_tres
    And hago una consulta a la tabla del MCP para tabla tx_Monetarias Carga Historica N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla tx_Monetarias Carga Historica N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla tx_Monetarias Carga Historica N_tres

  Scenario: Limpieza ambientacion para tx_Monetarias Carga Historica "Reprocesar un archivo, ya procesado"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias Carga Historica N_tres