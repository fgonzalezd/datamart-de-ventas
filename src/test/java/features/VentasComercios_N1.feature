@ventasComercios_N1

Feature: Ventas_Comercios N1 - Datos para la tabla vista

  Scenario: Ambientacion para "Datos para la tabla vista"
    When se debe limpiar el ambiente para la ejecucion de pruebas de Ventas_Comercios caso N_uno
    Then se debe insertar el dataset para las tablas de CUR de Ventas_Comercios caso N_uno

  Scenario: Validacion para tabla Ventas_Comercios "Datos para la tabla vista"
    When hago una consulta a la tabla LOB para vista Ventas_Comercios caso N_uno
    Then valido la visualizacion sea correcta para vista Ventas_Comercios caso N_uno

  Scenario: Limpieza ambientacion para "Datos para la tabla vista"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Ventas_Comercios caso N_uno