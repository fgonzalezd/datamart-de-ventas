@fechaCalendario_N1

  Feature: Fecha_Calendario N1 - La fecha de los datos no ha sido ejecutada

    Scenario: Ambientacion para "La fecha de los datos no ha sido ejecutada"
      Given se debe limpiar el ambiente para la ejecucion de pruebas para Fecha_Calendario caso N_uno
      When se debe insertar el dataset para las tablas de RAW para Fecha_Calendario caso N_uno
      And se debe insertar el dataset para las tablas de CUR para Fecha_Calendario caso N_uno
      Then se debe ejecutar el proceso de Fecha_Calendario caso N_uno

    Scenario: Validacion para tabla Fecha_Calendario "La fecha de los datos no ha sido ejecutada"
      When hago una consulta a la tabla del CUR para tabla Fecha_Calendario caso N_uno
      And hago una consulta a la tabla del MCP para tabla Fecha_Calendario caso N_uno
      Then valido que la insercion a la tabla CUR sea correcta para tabla Fecha_Calendario caso N_uno
      And valido que el registro de insercion en el MCP fue correcto para tabla Fecha_Calendario caso N_uno

    Scenario: Validacion de logs Fecha_Calendario "La fecha de los datos no ha sido ejecutada"
      When hago una consulta a los logs generados en RAW para Fecha_Calendario caso N_uno
      Then valido que la cantidad de logs generados sea correcto para Fecha_Calendario caso N_uno

    Scenario: Limpieza ambientacion para "La fecha de los datos no ha sido ejecutada"
      Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Fecha_Calendario caso N_uno