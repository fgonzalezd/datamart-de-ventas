@tipoContacto_N7

Feature: Tipo_Contacto N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para Tipo_Contacto "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Tipo_Contacto caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para Tipo_Contacto caso N_Siete
    Then se debe ejecutar el proceso de extraccion y transferencia para Tipo_Contacto caso N_Siete

  Scenario: Validacion de DRP Tipo_Contacto "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del master Tipo_Contacto caso N_Siete
    And hago una consulta al checksum del slave Tipo_Contacto caso N_Siete
    Then valido que el archivo transferido este correcto Tipo_Contacto caso N_Siete

  Scenario: Validacion de archivos en hdfs para Tipo_Contacto "Transferencia de archivo unico DRP"
    When hago una consulta a los archivos en el hdfs del master para Tipo_Contacto caso N_Siete
    And hago una consulta a los archivos en el hdfs del slave para Tipo_Contacto caso N_Siete
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Tipo_Contacto caso N_Siete
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Tipo_Contacto caso N_Siete

  Scenario: Validacion de logs Tipo_Contacto "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados del master para Tipo_Contacto caso N_Siete
    And hago una consulta a los logs generados del slave para Tipo_Contacto caso N_Siete
    Then valido que la cantidad de logs generados del master sea correcto para Tipo_Contacto caso N_Siete
    And valido que la cantidad de logs generados del slave sea correcto para Tipo_Contacto caso N_Siete

  Scenario: Limpieza ambientacion para Tipo_Contacto "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Tipo_Contacto caso N_Siete