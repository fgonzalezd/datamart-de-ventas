package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentArc_Page;
import pages.InsertEeccSpa_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class EccSpa_N8_Def {

    public static PropertiesInit prop;
    public static EnvironmentArc_Page environment;
    public static InsertEeccSpa_Page page;
    private String[] origenCS = new String[2];
    private String[] destinoCS = new String[2];
    public ArrayList<String> logs = new ArrayList<>();
    public ArrayList<String> logSlave = new ArrayList<>();

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para EECC_SPA caso N_Ocho$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropEECC_SPA());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createEECC_SPA());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawArc(), environment.deleteEECC_SPA());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL_EECC_SPA());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.logEECC_SPA());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsEECC_SPA());

        page.jdbcConnector(prop.HiveDriver(), prop.SshUrlSlave(), prop.SshUserSlave(), prop.SshPassSlave(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), environment.hdfsEECC_SPA());
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), environment.logEECC_SPA());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para EECC_SPA caso N_Ocho$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N1());
    }

    @Then("^se debe ejecutar el proceso de extraccion y transferencia para EECC_SPA caso N_Ocho$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_y_transferencia_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 70 20190709 ARC N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 70 20190710 ARC N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 72 20190709 ARC N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 72 20190710 ARC N");
    }

    @When("^hago una consulta al checksum del master EECC_SPA caso N_Ocho$")
    public void hago_una_consulta_al_checksum_del_master_EECC_SPA_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/eecc_spa/TO_TRANSFER/");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (int i = 0; i < 2; i++) {
                origenCS[i] = reader.readLine();
                System.out.println(origenCS[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @When("^hago una consulta al checksum del slave EECC_SPA caso N_Ocho$")
    public void hago_una_consulta_al_checksum_del_slave_EECC_SPA_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/eecc_spa/TO_IMPORT/");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (int i = 0; i < 2; i++) {
                destinoCS[i] = reader.readLine();
                System.out.println(destinoCS[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que el archivo transferido este correcto EECC_SPA caso N_Ocho$")
    public void valido_que_el_archivo_transferido_este_correcto_EECC_SPA_caso_N_Ocho() throws Throwable {
        for (int i = 0; i < 2; i++) {
            Reporter.addStepLog("Checksum Master= " + origenCS[i]);
            Reporter.addStepLog("Checksum Slave = " + destinoCS[i]);
            Reporter.addStepLog("------------------------------------------------------------------------------");
            assertEquals(origenCS[i], origenCS[i]);
        }
    }

    @When("^hago una consulta a los archivos en el hdfs del master para EECC_SPA caso N_Ocho$")
    public void hago_una_consulta_a_los_archivos_en_el_hdfs_del_master_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        
    }

    @When("^hago una consulta a los archivos en el hdfs del slave para EECC_SPA caso N_Ocho$")
    public void hago_una_consulta_a_los_archivos_en_el_hdfs_del_slave_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        
    }

    @Then("^valido que la cantidad de archivos en el hdfs del master sea correcta para EECC_SPA caso N_Ocho$")
    public void valido_que_la_cantidad_de_archivos_en_el_hdfs_del_master_sea_correcta_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        
    }

    @Then("^valido que la cantidad de archivos en el hdfs del slave sea correcta para EECC_SPA caso N_Ocho$")
    public void valido_que_la_cantidad_de_archivos_en_el_hdfs_del_slave_sea_correcta_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        
    }

    @When("^hago una consulta a los logs generados del master para EECC_SPA caso N_Ocho$")
    public void hago_una_consulta_a_los_logs_generados_del_master_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/arc/eecc_spa -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            String find = "";
            while ((find = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + find);
                logs.add(find);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @When("^hago una consulta a los logs generados del slave para EECC_SPA caso N_Ocho$")
    public void hago_una_consulta_a_los_logs_generados_del_slave_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/arc/eecc_spa -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String finds = "";
            while ((finds = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + finds);
                logSlave.add(finds);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados del master sea correcto para EECC_SPA caso N_Ocho$")
    public void valido_que_la_cantidad_de_logs_generados_del_master_sea_correcto_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        assertEquals(logs, logs);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logs + " de 3");
    }

    @Then("^valido que la cantidad de logs generados del slave sea correcto para EECC_SPA caso N_Ocho$")
    public void valido_que_la_cantidad_de_logs_generados_del_slave_sea_correcto_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        assertEquals(logSlave, logSlave);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logSlave + " de 3");
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para EECC_SPA caso N_Ocho$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_EECC_SPA_caso_N_Ocho() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropEECC_SPA());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsEECC_SPA());

        page.jdbcConnector(prop.HiveDriver(), prop.SshUrlSlave(), prop.SshUserSlave(), prop.SshPassSlave(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), environment.hdfsEECC_SPA());
    }
}
