@ctssTipoServicios_N9

Feature: Ctss_Tipo_Servicios N9 - Ingreso de fecha erronea en el parametro

  Scenario: Ejecucion del proceso extraccion Ctss_Tipo_Servicios "Ingreso de fecha erronea en el parametro"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Ctss_Tipo_Servicios caso N_Nueve
    When Se ejecuta el proceso de extraccion con una fecha erronea para Ctss_Tipo_Servicios caso N_Nueve
    Then se valida que el proceso de extraccion se haya terminado con error Ctss_Tipo_Servicios caso N_Nueve

  Scenario: Ejecucion del proceso STG Ctss_Tipo_Servicios "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga STG con una fecha erronea para Ctss_Tipo_Servicios caso N_Nueve
    Then se valida que el proceso de carga STG se haya terminado con error Ctss_Tipo_Servicios caso N_Nueve

  Scenario: Ejecucion del proceso RAW Ctss_Tipo_Servicios "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga RAW con una fecha erronea para Ctss_Tipo_Servicios caso N_Nueve
    Then se valida que el proceso de carga RAW se haya terminado con error Ctss_Tipo_Servicios caso N_Nueve

  Scenario: Validacion de logs Ctss_Tipo_Servicios "Ingreso de fecha erronea en el parametro"
    When hago una consulta a los logs generados en RAW para Ctss_Tipo_Servicios caso N_Nueve
    Then valido que la cantidad de logs generados sea correcto para Ctss_Tipo_Servicios caso N_Nueve