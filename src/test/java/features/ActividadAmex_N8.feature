@actividadAmex_N8

Feature: Actividad_Amex N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Actividad_Amex caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Actividad_Amex caso N_Ocho
    Then se debe ejecutar el proceso de extraccion para Actividad_Amex caso N_Ocho

  Scenario: Validacion de DRP Actividad_Amex "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum de los archivos de origen Actividad_Amex caso N_Ocho
    And hago una consulta al checksum de los archivos transferidos Actividad_Amex caso N_Ocho
    Then valido que los archivos transferidos esten correcto Actividad_Amex caso N_Ocho

  Scenario: Limpieza ambientacion para "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Actividad_Amex caso N_Ocho