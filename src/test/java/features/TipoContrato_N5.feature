@tipoContrato_N5

Feature: Tipo_Contrato N5 - Metrica activa sin regla

  Scenario: Ambientacion para Tipo_Contrato "Metrica activa sin regla"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Tipo_Contrato caso N_cinco
    When se debe insertar el dataset para las tablas de Oracle para Tipo_Contrato caso N_cinco
    And se debe insertar el dataset para las tablas de hive para Tipo_Contrato caso N_cinco
    Then se debe ejecutar el proceso de extraccion para Tipo_Contrato caso N_cinco

  Scenario: Validacion para tabla Tipo_Contrato "Metrica activa sin regla"
    When hago una consulta a la tabla del RAW para tabla Tipo_Contrato caso N_cinco
    And hago una consulta a la tabla del MCP para tabla Tipo_Contrato caso N_cinco
    Then valido que la insercion a la tabla RAW sea correcta para tabla Tipo_Contrato caso N_cinco
    And valido que el registro de insercion en el MCP fue correcto para tabla Tipo_Contrato caso N_cinco

  Scenario: Validacion de archivos en hdfs para Tipo_Contrato "Metrica activa sin regla"
    When hago una consulta a los archivos en TO_IMPORT para Tipo_Contrato caso N_cinco
    And hago una consulta a los archivos en TO_TRANSFER para Tipo_Contrato caso N_cinco
    And hago una consulta a los archivos en LOADING para Tipo_Contrato caso N_cinco
    Then valido que la cantidad de archivos en el hdfs sea correcta para Tipo_Contrato caso N_cinco

  Scenario: Validacion de logs Tipo_Contrato "Metrica activa sin regla"
    When hago una consulta a los logs generados en RAW para Tipo_Contrato caso N_cinco
    Then valido que la cantidad de logs generados sea correcto para Tipo_Contrato caso N_cinco

  Scenario: Limpieza ambientacion para Tipo_Contrato "Metrica activa sin regla"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Tipo_Contrato caso N_cinco