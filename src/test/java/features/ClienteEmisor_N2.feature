@clienteEmisor_N2

Feature: Cliente_Emisor N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para Cliente_Emisor "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Cliente_Emisor caso N_dos
    When se debe insertar el dataset para las tablas de Oracle para Cliente_Emisor caso N_dos
    And se debe insertar el dataset para las tablas de hive para Cliente_Emisor caso N_dos
    Then se debe ejecutar el proceso de extraccion para Cliente_Emisor caso N_dos

  Scenario: Validacion para tabla Cliente_Emisor "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del RAW para tabla Cliente_Emisor caso N_dos
    And hago una consulta a la tabla del MCP para tabla Cliente_Emisor caso N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla Cliente_Emisor caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla Cliente_Emisor caso N_dos

  Scenario: Validacion de archivos en hdfs para Cliente_Emisor "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los archivos en TO_IMPORT para Cliente_Emisor caso N_dos
    And hago una consulta a los archivos en TO_TRANSFER para Cliente_Emisor caso N_dos
    And hago una consulta a los archivos en LOADING para Cliente_Emisor caso N_dos
    Then valido que la cantidad de archivos en el hdfs sea correcta para Cliente_Emisor caso N_dos

  Scenario: Validacion de logs Cliente_Emisor "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los logs generados en RAW para Cliente_Emisor caso N_dos
    Then valido que la cantidad de logs generados sea correcto para Cliente_Emisor caso N_dos

  Scenario: Limpieza ambientacion para Cliente_Emisor "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Cliente_Emisor caso N_dos