package runnings;

import com.vimalselvam.cucumber.listener.ExtentProperties;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",
        glue = "definitions",
        plugin = {
                "pretty", "html:target/cucumber" ,
                "json:target/cucumber.json",
                "com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:"
        }
)

public class Running {

    public static SimpleDateFormat sdf;
    @AfterClass
    public static void setup() {
        //Carga la config del xml
        System.out.println("CARGA CONFIGURACION DEL XML");
        Reporter.loadXMLConfig(new File("./src/test/resources/config/extent-config.xml"));

        // Detalle caracteristicas
        Reporter.setSystemInfo("Nombre Proyecto","TBK Datamart de ventas");
        Reporter.setSystemInfo("Zona Horaria", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Ubicacion Usuario", System.getProperty("user.country"));
        Reporter.setSystemInfo("Nombre SO", System.getProperty("os.name"));
    }

    @BeforeClass
    public static void setupPath() {
        sdf = new SimpleDateFormat("dd-MM-YYYY_hh-mm-ss");
        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
        extentProperties.setReportPath(System.getProperty(
                "user.dir") + "/reportes" + "/Reporte_Automatizacion_DMVTAS " + sdf.format(new Date()) + ".html");
    }
}
