@clienteEmisor_N8

Feature: Cliente_Emisor N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para Cliente_Emisor "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Cliente_Emisor caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Cliente_Emisor caso N_Ocho
    Then se debe ejecutar el proceso de extraccion y transferencia para Cliente_Emisor caso N_Ocho

  Scenario: Validacion de DRP Cliente_Emisor "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum del master Cliente_Emisor caso N_Ocho
    And hago una consulta al checksum del slave Cliente_Emisor caso N_Ocho
    Then valido que el archivo transferido este correcto Cliente_Emisor caso N_Ocho

  Scenario: Validacion de archivos en hdfs para Cliente_Emisor "Transferencia de multiples archivos DRP"
    When hago una consulta a los archivos en el hdfs del master para Cliente_Emisor caso N_Ocho
    And hago una consulta a los archivos en el hdfs del slave para Cliente_Emisor caso N_Ocho
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Cliente_Emisor caso N_Ocho
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Cliente_Emisor caso N_Ocho

  Scenario: Validacion de logs Cliente_Emisor "Transferencia de multiples archivos DRP"
    When hago una consulta a los logs generados del master para Cliente_Emisor caso N_Ocho
    And hago una consulta a los logs generados del slave para Cliente_Emisor caso N_Ocho
    Then valido que la cantidad de logs generados del master sea correcto para Cliente_Emisor caso N_Ocho
    And valido que la cantidad de logs generados del slave sea correcto para Cliente_Emisor caso N_Ocho


  Scenario: Limpieza ambientacion para Cliente_Emisor "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Cliente_Emisor caso N_Ocho