@ctssTipoServicios_N7

Feature: Ctss_Tipo_Servicios N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para Ctss_Tipo_Servicios "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Ctss_Tipo_Servicios caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para Ctss_Tipo_Servicios caso N_Siete
    Then se debe ejecutar el proceso de extraccion y transferencia para Ctss_Tipo_Servicios caso N_Siete

  Scenario: Validacion de DRP Ctss_Tipo_Servicios "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del master Ctss_Tipo_Servicios caso N_Siete
    And hago una consulta al checksum del slave Ctss_Tipo_Servicios caso N_Siete
    Then valido que el archivo transferido este correcto Ctss_Tipo_Servicios caso N_Siete

  Scenario: Validacion de archivos en hdfs para Ctss_Tipo_Servicios "Transferencia de archivo unico DRP"
    When hago una consulta a los archivos en el hdfs del master para Ctss_Tipo_Servicios caso N_Siete
    And hago una consulta a los archivos en el hdfs del slave para Ctss_Tipo_Servicios caso N_Siete
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Ctss_Tipo_Servicios caso N_Siete
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Ctss_Tipo_Servicios caso N_Siete

  Scenario: Validacion de logs Ctss_Tipo_Servicios "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados del master para Ctss_Tipo_Servicios caso N_Siete
    And hago una consulta a los logs generados del slave para Ctss_Tipo_Servicios caso N_Siete
    Then valido que la cantidad de logs generados del master sea correcto para Ctss_Tipo_Servicios caso N_Siete
    And valido que la cantidad de logs generados del slave sea correcto para Ctss_Tipo_Servicios caso N_Siete

  Scenario: Limpieza ambientacion para Ctss_Tipo_Servicios "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Ctss_Tipo_Servicios caso N_Siete