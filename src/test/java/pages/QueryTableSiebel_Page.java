package pages;

public class QueryTableSiebel_Page extends Base_Page{

    public static String queryXML = "src/test/resources/query/QueryTable_Siebel.xml";

    /*********************
     ***** QUERY RAW *****
     *********************/

    public static String querySPARTYPER() {
        String query = fetchQuery(queryXML).getProperty("querySPARTYPER");
        return query;
    }

    public static String queryCOUNT_SPARTYPER() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_SPARTYPER");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_SPARTYPER() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_SPARTYPER");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_SPARTYPER() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_SPARTYPER");
        return query;
    }
}
