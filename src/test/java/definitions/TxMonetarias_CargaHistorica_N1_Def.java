package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentTxs_Page;
import pages.QueryTableTxs_Page;
import pages.TxMonetariasInsert_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TxMonetarias_CargaHistorica_N1_Def {

    public static PropertiesInit prop;
    public static EnvironmentTxs_Page environment;
    public static TxMonetariasInsert_Page page;
    public static QueryTableTxs_Page query;
    public static String insertXML = "src/test/resources/insert/Insert_CargaHistorica.xml";
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvRAW = "src/test/resources/dataset/CargaHistorica/cargaHistoricaRAW_N1.csv";
    public static String csvMCP = "src/test/resources/dataset/CargaHistorica/cargaHistoricaMCP_N1.csv";
    public ArrayList<String[]> destinoRAW = new ArrayList<>();
    public ArrayList<String[]> datasetRAW = new ArrayList<>();
    public ArrayList<String[]> destinoMCP = new ArrayList<>();
    public ArrayList<String[]> datasetMCP = new ArrayList<>();
    public String destinoCS;

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias Carga Historica N_uno$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "rm /data/environment/hdp_app/load_raw/cargaHistoricaCSV.csv");
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm /data/environment/hdp_lake/cargaHistoricaCSV.csv");
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaStgTxs(), "ALTER TABLE stg_txs.tx_monetarias_his DROP PARTITION(id_anio_mes=201802)");
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -R /data/environment/hdp_lake/stg_txs/tx_monetarias_his/LOADING/*");
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), environment.deleteTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N1(insertXML));
    }

    @When("^transfiero el archivo a una carpeta del servidor para tx_Monetarias Carga Historica N_uno$")
    public void transfiero_el_archivo_a_una_carpeta_del_servidor_para_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelSftp sftpChannel = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            sftpChannel = (ChannelSftp) sessionSSH.openChannel("sftp");
            sftpChannel.connect();

            // executar comando
            sftpChannel.put((System.getProperty("user.dir") + "/src/test/resources/dataset/CargaHistorica/cargaHistoricaCSV.csv"), "/data/environment/hdp_app/load_raw/cargaHistoricaCSV.csv");

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (sftpChannel != null) {
                sftpChannel.isClosed();
            }
        }
    }

    @When("^muevo el archivo a una ruta hdfs para tx_Monetarias Carga Historica N_uno$")
    public void muevo_el_archivo_a_una_ruta_hdfs_para_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "hdfs dfs -put /data/environment/hdp_app/load_raw/cargaHistoricaCSV.csv /data/environment/hdp_lake/cargaHistoricaCSV.csv");
    }

    @When("^consulto el checksum del archivo transferido para tx_Monetarias Carga Historica N_uno$")
    public void consulto_el_checksum_del_archivo_transferido_para_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/cargaHistoricaCSV.csv");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((destinoCS = reader.readLine()) != null) {
                System.out.println(++index + " : " + destinoCS);
                Reporter.addStepLog(destinoCS);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido el checksum de ambos archivos para tx_Monetarias Carga Historica N_uno$")
    public void valido_el_checksum_de_ambos_archivos_para_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        //TODO agregar equals
//        assertEquals("", destinoCS);
    }

    @Then("^se ejecuta el proceso para tx_Monetarias Carga Historica N_uno$")
    public void se_ejecuta_el_proceso_para_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_txs_monetaria_his.sh 20180201 20180228 TXS /data/environment/hdp_lake cargaHistoricaCSV.csv");
    }

    @When("^hago una consulta a la tabla del RAW para tabla tx_Monetarias Carga Historica N_uno$")
    public void hago_una_consulta_a_la_tabla_del_RAW_para_tabla_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaRawTxs());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryTX_MONETARIAS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoRAW.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvRAW))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetRAW.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("^hago una consulta a la tabla del MCP para tabla tx_Monetarias Carga Historica N_uno$")
    public void hago_una_consulta_a_la_tabla_del_MCP_para_tabla_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaMCP());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_TX_MONETARIAS_HIST());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoMCP.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvMCP))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetMCP.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido que la insercion a la tabla RAW sea correcta para tabla tx_Monetarias Carga Historica N_uno$")
    public void valido_que_la_insercion_a_la_tabla_RAW_sea_correcta_para_tabla_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryTX_MONETARIAS_COUNT());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(datasetRAW.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino RAW " + i + " = " + Arrays.deepToString(destinoRAW.get(i)));
            System.out.println("Dataset RAW " + i + " = " + Arrays.deepToString(datasetRAW.get(i)));
//            assertTrue(Arrays.deepToString(destinoRAW.get(i)).equals(Arrays.deepToString(datasetRAW.get(i))));
        }
        System.out.println(" ");
        destinoRAW.clear();
        datasetRAW.clear();
    }

    @Then("^valido que el registro de insercion en el MCP fue correcto para tabla tx_Monetarias Carga Historica N_uno$")
    public void valido_que_el_registro_de_insercion_en_el_MCP_fue_correcto_para_tabla_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_COUNT_TX_MONETARIAS_HIST());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(datasetMCP.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            System.out.println("Dataset MCP " + i + " = " + Arrays.deepToString(datasetMCP.get(i)));
//            assertTrue(Arrays.deepToString(destinoMCP.get(i)).equals(Arrays.deepToString(datasetMCP.get(i))));
        }
        System.out.println(" ");
        destinoMCP.clear();
        datasetMCP.clear();
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias Carga Historica N_uno$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_tx_Monetarias_Carga_Historica_N_uno() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "rm /data/environment/hdp_app/load_raw/cargaHistoricaCSV.csv");
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm /data/environment/hdp_lake/cargaHistoricaCSV.csv");
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaStgTxs(), "ALTER TABLE stg_txs.tx_monetarias_his DROP PARTITION(id_anio_mes=201802)");
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -R /data/environment/hdp_lake/stg_txs/tx_monetarias_his/LOADING/*");
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), environment.deleteTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
    }
}
