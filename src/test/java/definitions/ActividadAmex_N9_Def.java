package definitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.Base_Page;
import properties.PropertiesInit;

public class ActividadAmex_N9_Def {

    public static PropertiesInit prop;
    public static Base_Page page;

    @When("^Se ejecuta el proceso de extraccion con una fecha erronea para Actividad_Amex caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_extraccion_con_una_fecha_erronea_para_Actividad_Amex_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 5 P0190709 ARC N");
    }

    @Then("^se valida que el proceso de extraccion se haya terminado con error Actividad_Amex caso N_Nueve$")
    public void se_valida_que_el_proceso_de_extraccion_se_haya_terminado_con_error_Actividad_Amex_caso_N_Nueve() throws Throwable {
        //TODO validar
    }

    @When("^Se ejecuta el proceso de carga STG con una fecha erronea para Actividad_Amex caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_STG_con_una_fecha_erronea_para_Actividad_Amex_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 6 S01907j9 ARC N");
    }

    @Then("^se valida que el proceso de carga STG se haya terminado con error Actividad_Amex caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_STG_se_haya_terminado_con_error_Actividad_Amex_caso_N_Nueve() throws Throwable {
        //TODO validar
    }

    @When("^Se ejecuta el proceso de carga RAW con una fecha erronea para Actividad_Amex caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_RAW_con_una_fecha_erronea_para_Actividad_Amex_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 8 2019:07:09 ARC N");
    }

    @Then("^se valida que el proceso de carga RAW se haya terminado con error Actividad_Amex caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_RAW_se_haya_terminado_con_error_Actividad_Amex_caso_N_Nueve() throws Throwable {
        //TODO validar
    }
}
