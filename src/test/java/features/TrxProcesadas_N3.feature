@trxProcesadas_N3

Feature: Transacciones_Procesadas N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Transacciones_Procesadas caso N_tres
    When se debe insertar el dataset para las tablas de RAW para Transacciones_Procesadas caso N_tres
    And se debe insertar el dataset para las tablas de CUR para Transacciones_Procesadas caso N_tres
    Then se debe ejecutar el proceso de extraccion con una fecha del 8/7/2019 para Transacciones_Procesadas caso N_tres

  Scenario: Validacion para tabla Transacciones_Procesadas "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del CUR para tabla Transacciones_Procesadas caso N_tres
    And hago una consulta a la tabla del MCP para tabla Transacciones_Procesadas caso N_tres
    Then valido que la insercion a la tabla CUR sea correcta para tabla Transacciones_Procesadas caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Transacciones_Procesadas caso N_tres

  Scenario: Validacion de logs Transacciones_Procesadas "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Transacciones_Procesadas caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Transacciones_Procesadas caso N_tres

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Transacciones_Procesadas caso N_tres