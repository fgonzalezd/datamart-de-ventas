package pages;

public class QueryTableCtss_Page extends Base_Page{

    public static String queryXML = "src/test/resources/query/QueryTable_Ctss.xml";

    /*********************
     ***** QUERY RAW *****
     *********************/

    public static String queryCTSS_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryCTSS_SERVICIOS");
        return query;
    }

    public static String queryCOUNT_CTSS_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_CTSS_SERVICIOS");
        return query;
    }

    public static String queryCTSS_TIPO_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryCTSS_TIPO_SERVICIOS");
        return query;
    }

    public static String queryCOUNT_CTSS_TIPO_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_CTSS_TIPO_SERVICIOS");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_CTSS_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_CTSS_SERVICIOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_CTSS_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_CTSS_SERVICIOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_CTSS_TIPO_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_CTSS_TIPO_SERVICIOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_CTSS_TIPO_SERVICIOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_CTSS_TIPO_SERVICIOS");
        return query;
    }
}
