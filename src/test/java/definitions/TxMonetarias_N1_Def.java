package definitions;

import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentTxs_Page;
import pages.QueryTableTxs_Page;
import pages.TxMonetariasInsert_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class TxMonetarias_N1_Def {

    public static PropertiesInit prop;
    public static EnvironmentTxs_Page environment;
    public static TxMonetariasInsert_Page page;
    public static QueryTableTxs_Page query;
    public static String insertXML = "src/test/resources/insert/Insert_txMonetarias.xml";
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvRAW = "src/test/resources/dataset/TxMonetarias/txMonetariasRAW_N1.csv";
    public static String csvMCP = "src/test/resources/dataset/TxMonetarias/txMonetariasMCP_N1.csv";
    public ArrayList<String[]> destinoRAW = new ArrayList<>();
    public ArrayList<String[]> datasetRAW = new ArrayList<>();
    public ArrayList<String[]> destinoMCP = new ArrayList<>();
    public ArrayList<String[]> datasetMCP = new ArrayList<>();

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias caso N_uno$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_tx_Monetarias_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropTX_MONETARIAS());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), environment.deleteTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para tx_Monetarias caso N_uno$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_tx_Monetarias_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN09(insertXML));
    }

    @When("^se debe insertar el dataset para las tablas de hive para tx_Monetarias caso N_uno$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_hive_para_tx_Monetarias_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), page.insert_RAW1(insertXML));
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), page.insert_RAW2(insertXML));
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N1(insertXML));
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO1_N1(insertXML));
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO2_N1(insertXML));
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO3_N1(insertXML));
    }

    @Then("^se debe ejecutar el proceso de extraccion con una fecha del (\\d+)/(\\d+)/(\\d+) para tx_Monetarias caso N_uno$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_con_una_fecha_del_para_tx_Monetarias_caso_N_uno(int arg1, int arg2, int arg3) throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 25 20190709 TXS N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 26 20190709 TXS N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 28 20190709 TXS N");
    }

    @When("^hago una consulta a la tabla del RAW para tabla tx_Monetarias caso N_uno$")
    public void hago_una_consulta_a_la_tabla_del_RAW_para_tabla_tx_Monetarias_caso_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaRawTxs());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryTX_MONETARIAS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoRAW.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvRAW))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetRAW.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("^hago una consulta a la tabla del MCP para tabla tx_Monetarias caso N_uno$")
    public void hago_una_consulta_a_la_tabla_del_MCP_para_tabla_tx_Monetarias_caso_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaMCP());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_TX_MONETARIAS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoMCP.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvMCP))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetMCP.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido que la insercion a la tabla RAW sea correcta para tabla tx_Monetarias caso N_uno$")
    public void valido_que_la_insercion_a_la_tabla_RAW_sea_correcta_para_tabla_tx_Monetarias_caso_N_uno() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryTX_MONETARIAS_COUNT());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(datasetRAW.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino RAW " + i + " = " + Arrays.deepToString(destinoRAW.get(i)));
            System.out.println("Dataset RAW " + i + " = " + Arrays.deepToString(datasetRAW.get(i)));
//            assertTrue(Arrays.deepToString(destinoRAW.get(i)).equals(Arrays.deepToString(datasetRAW.get(i))));
        }
        System.out.println(" ");
        destinoRAW.clear();
        datasetRAW.clear();
    }

    @Then("^valido que el registro de insercion en el MCP fue correcto para tabla tx_Monetarias caso N_uno$")
    public void valido_que_el_registro_de_insercion_en_el_MCP_fue_correcto_para_tabla_tx_Monetarias_caso_N_uno() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_COUNT_TX_MONETARIAS());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(datasetMCP.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            System.out.println("Dataset MCP " + i + " = " + Arrays.deepToString(datasetMCP.get(i)));
//            assertTrue(Arrays.deepToString(destinoMCP.get(i)).equals(Arrays.deepToString(datasetMCP.get(i))));
        }
        System.out.println(" ");
        destinoMCP.clear();
        datasetMCP.clear();
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias caso N_uno$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_tx_Monetarias_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTxs(), environment.deleteTX_MONETARIAS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
    }
}
