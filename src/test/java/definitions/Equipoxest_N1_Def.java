package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentBpos_Page;
import pages.InsertEquipoxest_Page;
import pages.QueryTableBpos_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class Equipoxest_N1_Def {

    public static PropertiesInit prop;
    public static EnvironmentBpos_Page environment;
    public static QueryTableBpos_Page query;
    public static InsertEquipoxest_Page page;
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvRAW = "src/test/resources/dataset/Equipoxest/equipoxestRAW_N1.csv";
    public static String csvMCP = "src/test/resources/dataset/Equipoxest/equipoxestMCP_N1.csv";
    public ArrayList<String[]> destinoRAW = new ArrayList<>();
    public ArrayList<String[]> datasetRAW = new ArrayList<>();
    public ArrayList<String[]> destinoMCP = new ArrayList<>();
    public ArrayList<String[]> datasetMCP = new ArrayList<>();
    public ArrayList<String> logs = new ArrayList<>();

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Equipoxest caso N_uno$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Equipoxest_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropEQUIPOXEST());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createEQUIPOXEST());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawBpos(), environment.deleteEQUIPOXEST());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL_EQUIPOXEST());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.logEQUIPOXEST());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsEQUIPOXEST());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para Equipoxest caso N_uno$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_Equipoxest_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN());
    }

    @When("^se debe insertar el dataset para las tablas de hive para Equipoxest caso N_uno$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_hive_para_Equipoxest_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawBpos(), page.insert_RAW1());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawBpos(), page.insert_RAW2());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N1());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO1_N1());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO2_N1());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO3_N1());
    }

    @Then("^se debe ejecutar el proceso de extraccion para Equipoxest caso N_uno$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_para_Equipoxest_caso_N_uno() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 78 20190709 BPOS N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 79 20190709 BPOS N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 81 20190709 BPOS N");
    }

    @When("^hago una consulta a la tabla del RAW para tabla Equipoxest caso N_uno$")
    public void hago_una_consulta_a_la_tabla_del_RAW_para_tabla_Equipoxest_caso_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaRawBpos());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEQUIPOXEST());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoRAW.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvRAW))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetRAW.add(line.split(", "));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("^hago una consulta a la tabla del MCP para tabla Equipoxest caso N_uno$")
    public void hago_una_consulta_a_la_tabla_del_MCP_para_tabla_Equipoxest_caso_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaMCP());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_EQUIPOXEST());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoMCP.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvMCP))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetMCP.add(line.split(", "));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido que la insercion a la tabla RAW sea correcta para tabla Equipoxest caso N_uno$")
    public void valido_que_la_insercion_a_la_tabla_RAW_sea_correcta_para_tabla_Equipoxest_caso_N_uno() throws Throwable {
        ResultSet resRows = stmtHive.executeQuery(query.queryCOUNT_EQUIPOXEST());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }

        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino RAW " + i + " = " + Arrays.deepToString(destinoRAW.get(i)));
            System.out.println("Dataset RAW " + i + " = " + Arrays.deepToString(datasetRAW.get(i)));
            assertEquals(Arrays.deepToString(destinoRAW.get(i)), Arrays.deepToString(destinoRAW.get(i)));
        }
        System.out.println(" ");
        destinoRAW.clear();
        datasetRAW.clear();
    }

    @Then("^valido que el registro de insercion en el MCP fue correcto para tabla Equipoxest caso N_uno$")
    public void valido_que_el_registro_de_insercion_en_el_MCP_fue_correcto_para_tabla_Equipoxest_caso_N_uno() throws Throwable {
        ResultSet resRows = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_COUNT_EQUIPOXEST());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }

        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            System.out.println("Dataset MCP " + i + " = " + Arrays.deepToString(datasetMCP.get(i)));
            assertEquals(Arrays.deepToString(destinoMCP.get(i)), Arrays.deepToString(destinoMCP.get(i)));
        }
        System.out.println(" ");
        destinoMCP.clear();
        datasetMCP.clear();
    }

    @When("^hago una consulta a los archivos en TO_IMPORT para Equipoxest caso N_uno$")
    public void hago_una_consulta_a_los_archivos_en_TO_IMPORT_para_Equipoxest_caso_N_uno() throws Throwable {
        
    }

    @When("^hago una consulta a los archivos en TO_TRANSFER para Equipoxest caso N_uno$")
    public void hago_una_consulta_a_los_archivos_en_TO_TRANSFER_para_Equipoxest_caso_N_uno() throws Throwable {
        
    }

    @When("^hago una consulta a los archivos en LOADING para Equipoxest caso N_uno$")
    public void hago_una_consulta_a_los_archivos_en_LOADING_para_Equipoxest_caso_N_uno() throws Throwable {
        
    }

    @Then("^valido que la cantidad de archivos en el hdfs sea correcta para Equipoxest caso N_uno$")
    public void valido_que_la_cantidad_de_archivos_en_el_hdfs_sea_correcta_para_Equipoxest_caso_N_uno() throws Throwable {

    }

    @When("^hago una consulta a los logs generados en RAW para Equipoxest caso N_uno$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Equipoxest_caso_N_uno() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/bpos/equipoxest -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String find = "";
            while ((find = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + find);
                logs.add(find);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Equipoxest caso N_uno$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Equipoxest_caso_N_uno() throws Throwable {
        assertEquals(logs, logs);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logs + " de 3");
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Equipoxest caso N_uno$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Equipoxest_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropEQUIPOXEST());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawBpos(), environment.deleteEQUIPOXEST());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL_EQUIPOXEST());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsEQUIPOXEST());
    }
}
