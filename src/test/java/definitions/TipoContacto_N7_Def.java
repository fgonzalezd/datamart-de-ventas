package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentArc_Page;
import pages.InsertTipoContacto_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TipoContacto_N7_Def {

    public static PropertiesInit prop;
    public static EnvironmentArc_Page environment;
    public static InsertTipoContacto_Page page;
    public ArrayList<String> origenCS = new ArrayList<>();
    public ArrayList<String> destinoCS = new ArrayList<>();
    public ArrayList<String> logs = new ArrayList<>();
    public ArrayList<String> logSlave = new ArrayList<>();

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Tipo_Contacto caso N_Siete$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropTIPO_CONTACTO());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createTIPO_CONTACTO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawArc(), environment.deleteTIPO_CONTACTO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL_TIPO_CONTACTO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.logTIPO_CONTACTO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsTIPO_CONTACTO());

        page.jdbcConnector(prop.HiveDriver(), prop.SshUrlSlave(), prop.SshUserSlave(), prop.SshPassSlave(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), environment.hdfsTIPO_CONTACTO());
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), environment.logTIPO_CONTACTO());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para Tipo_Contacto caso N_Siete$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N1());
    }

    @Then("^se debe ejecutar el proceso de extraccion y transferencia para Tipo_Contacto caso N_Siete$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_y_transferencia_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 94 20190710 ARC N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 96 20190710 ARC N");
    }

    @When("^hago una consulta al checksum del master Tipo_Contacto caso N_Siete$")
    public void hago_una_consulta_al_checksum_del_master_Tipo_Contacto_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/tipo_contacto/TO_TRANSFER/tipo_contacto_20190710-m-00000");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String cksum = "";
            while ((cksum = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + cksum);
                origenCS.add(cksum);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }

    }

    @When("^hago una consulta al checksum del slave Tipo_Contacto caso N_Siete$")
    public void hago_una_consulta_al_checksum_del_slave_Tipo_Contacto_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/tipo_contacto/TO_IMPORT/tipo_contacto_20190710-m-00000");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String cksum = "";
            while ((cksum = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + cksum);
                destinoCS.add(cksum);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que el archivo transferido este correcto Tipo_Contacto caso N_Siete$")
    public void valido_que_el_archivo_transferido_este_correcto_Tipo_Contacto_caso_N_Siete() throws Throwable {
        Reporter.addStepLog("Checksum Master= " + origenCS);
        Reporter.addStepLog("Checksum Slave = " + destinoCS);
        assertEquals(origenCS, origenCS);
    }

    @When("^hago una consulta a los archivos en el hdfs del master para Tipo_Contacto caso N_Siete$")
    public void hago_una_consulta_a_los_archivos_en_el_hdfs_del_master_para_Tipo_Contacto_caso_N_Siete() throws Throwable {

    }

    @When("^hago una consulta a los archivos en el hdfs del slave para Tipo_Contacto caso N_Siete$")
    public void hago_una_consulta_a_los_archivos_en_el_hdfs_del_slave_para_Tipo_Contacto_caso_N_Siete() throws Throwable {

    }

    @Then("^valido que la cantidad de archivos en el hdfs del master sea correcta para Tipo_Contacto caso N_Siete$")
    public void valido_que_la_cantidad_de_archivos_en_el_hdfs_del_master_sea_correcta_para_Tipo_Contacto_caso_N_Siete() throws Throwable {

    }

    @Then("^valido que la cantidad de archivos en el hdfs del slave sea correcta para Tipo_Contacto caso N_Siete$")
    public void valido_que_la_cantidad_de_archivos_en_el_hdfs_del_slave_sea_correcta_para_Tipo_Contacto_caso_N_Siete() throws Throwable {

    }

    @When("^hago una consulta a los logs generados del master para Tipo_Contacto caso N_Siete$")
    public void hago_una_consulta_a_los_logs_generados_del_master_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/arc/tipo_contacto -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            String find = "";
            while ((find = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + find);
                logs.add(find);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @When("^hago una consulta a los logs generados del slave para Tipo_Contacto caso N_Siete$")
    public void hago_una_consulta_a_los_logs_generados_del_slave_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/arc/tipo_contacto -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String finds = "";
            while ((finds = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + finds);
                logSlave.add(finds);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados del master sea correcto para Tipo_Contacto caso N_Siete$")
    public void valido_que_la_cantidad_de_logs_generados_del_master_sea_correcto_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        assertEquals(logs, logs);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logs + " de 3");
    }

    @Then("^valido que la cantidad de logs generados del slave sea correcto para Tipo_Contacto caso N_Siete$")
    public void valido_que_la_cantidad_de_logs_generados_del_slave_sea_correcto_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        assertEquals(logSlave, logSlave);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logSlave + " de 3");
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Tipo_Contacto caso N_Siete$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Tipo_Contacto_caso_N_Siete() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropTIPO_CONTACTO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsTIPO_CONTACTO());

        page.jdbcConnector(prop.HiveDriver(), prop.SshUrlSlave(), prop.SshUserSlave(), prop.SshPassSlave(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), environment.hdfsTIPO_CONTACTO());
    }
}
