@direcciones_N7

Feature: Direcciones N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para Direcciones "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Direcciones caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para Direcciones caso N_Siete
    Then se debe ejecutar el proceso de extraccion y transferencia para Direcciones caso N_Siete

  Scenario: Validacion de DRP Direcciones "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del master Direcciones caso N_Siete
    And hago una consulta al checksum del slave Direcciones caso N_Siete
    Then valido que el archivo transferido este correcto Direcciones caso N_Siete

  Scenario: Validacion de archivos en hdfs para Direcciones "Transferencia de archivo unico DRP"
    When hago una consulta a los archivos en el hdfs del master para Direcciones caso N_Siete
    And hago una consulta a los archivos en el hdfs del slave para Direcciones caso N_Siete
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Direcciones caso N_Siete
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Direcciones caso N_Siete

  Scenario: Validacion de logs Direcciones "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados del master para Direcciones caso N_Siete
    And hago una consulta a los logs generados del slave para Direcciones caso N_Siete
    Then valido que la cantidad de logs generados del master sea correcto para Direcciones caso N_Siete
    And valido que la cantidad de logs generados del slave sea correcto para Direcciones caso N_Siete

  Scenario: Limpieza ambientacion para Direcciones "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Direcciones caso N_Siete