@tipoCambio_N7

Feature: Tipo_Cambio N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Tipo_Cambio caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para Tipo_Cambio caso N_Siete
    Then se debe ejecutar el proceso de extraccion con una fecha del 10/7/2019 para Tipo_Cambio caso N_Siete

  Scenario: Validacion de DRP Tipo_Cambio "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del archivo origen Tipo_Cambio caso N_Siete
    And hago una consulta al checksum del archivo transferido Tipo_Cambio caso N_Siete
    Then valido que el archivo transferido este correcto Tipo_Cambio caso N_Siete

  Scenario: Validacion de logs Tipo_Cambio "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados en RAW para Tipo_Cambio caso N_Siete
    Then valido que la cantidad de logs generados sea correcto para Tipo_Cambio caso N_Siete

  Scenario: Limpieza ambientacion para "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Tipo_Cambio caso N_Siete