package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentArc_Page;
import pages.EnvironmentParcom_Page;
import pages.InsertComisionRetencion_Page;
import pages.RubrosInsert_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

public class CobroComision_N7_Def {

    public static PropertiesInit prop;
    public static EnvironmentParcom_Page environment;
    public static InsertComisionRetencion_Page page;
    public String origenCS;
    public String destinoCS;
    public String logs;

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Cobro_Comision_Retencion caso N_Siete$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/LOADING/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_TRANSFER/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_IMPORT/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/LOADING/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_TRANSFER/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropCOMISION_RETENCION());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createCOMISION_RETENCION());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.jdbcConnector(prop.HiveDriver(), prop.SshUrlSlave(), prop.SshUserSlave(), prop.SshPassSlave(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), "rm -R /data/environment/hdp_log/load_raw/parcom/cobros_comision_retencion");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "rm -R /data/environment/hdp_log/load_raw/parcom/cobros_comision_retencion");
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para Cobro_Comision_Retencion caso N_Siete$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N1());
    }

    @Then("^se debe ejecutar el proceso de extraccion con una fecha del (\\d+)/(\\d+)/(\\d+) para Cobro_Comision_Retencion caso N_Siete$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_con_una_fecha_del_para_Cobro_Comision_Retencion_caso_N_Siete(int arg1, int arg2, int arg3) throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 36 20190710 PARCOM N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 38 20190710 PARCOM N");
    }

    @When("^hago una consulta al checksum del archivo origen Cobro_Comision_Retencion caso N_Siete$")
    public void hago_una_consulta_al_checksum_del_archivo_origen_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_TRANSFER/cobros_comision_retencion_20190710-m-00000");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((origenCS = reader.readLine()) != null) {
                System.out.println(++index + " : " + origenCS);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @When("^hago una consulta al checksum del archivo transferido Cobro_Comision_Retencion caso N_Siete$")
    public void hago_una_consulta_al_checksum_del_archivo_transferido_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_IMPORT/cobros_comision_retencion_20190710-m-00000");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((destinoCS = reader.readLine()) != null) {
                System.out.println(++index + " : " + destinoCS);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que el archivo transferido este correcto Cobro_Comision_Retencion caso N_Siete$")
    public void valido_que_el_archivo_transferido_este_correcto_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        Reporter.addStepLog("Checksum Master= " + origenCS);
        Reporter.addStepLog("Checksum Slave = " + destinoCS);
//        assertEquals(origenCS, destinoCS);
    }

    @When("^hago una consulta a los logs generados en RAW para Cobro_Comision_Retencion caso N_Siete$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/parcom/cobros_comision_retencion/ -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((logs = reader.readLine()) != null) {
                System.out.println(++index + " : " + logs);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Cobro_Comision_Retencion caso N_Siete$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        assertEquals("1", logs);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logs + " de 1");
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Cobro_Comision_Retencion caso N_Siete$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Cobro_Comision_Retencion_caso_N_Siete() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/LOADING/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_TRANSFER/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_IMPORT/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/LOADING/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_TRANSFER/* ; hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/cobros_comision_retencion/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropCOMISION_RETENCION());
    }
}
