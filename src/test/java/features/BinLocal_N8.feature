@binLocal_N8

Feature: Bin_Local N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para Bin_Local "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Bin_Local caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Bin_Local caso N_Ocho
    Then se debe ejecutar el proceso de extraccion y transferencia para Bin_Local caso N_Ocho

  Scenario: Validacion de DRP Bin_Local "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum del master Bin_Local caso N_Ocho
    And hago una consulta al checksum del slave Bin_Local caso N_Ocho
    Then valido que el archivo transferido este correcto Bin_Local caso N_Ocho

  Scenario: Validacion de archivos en hdfs para Bin_Local "Transferencia de multiples archivos DRP"
    When hago una consulta a los archivos en el hdfs del master para Bin_Local caso N_Ocho
    And hago una consulta a los archivos en el hdfs del slave para Bin_Local caso N_Ocho
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Bin_Local caso N_Ocho
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Bin_Local caso N_Ocho

  Scenario: Validacion de logs Bin_Local "Transferencia de multiples archivos DRP"
    When hago una consulta a los logs generados del master para Bin_Local caso N_Ocho
    And hago una consulta a los logs generados del slave para Bin_Local caso N_Ocho
    Then valido que la cantidad de logs generados del master sea correcto para Bin_Local caso N_Ocho
    And valido que la cantidad de logs generados del slave sea correcto para Bin_Local caso N_Ocho


  Scenario: Limpieza ambientacion para Bin_Local "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Bin_Local caso N_Ocho