@fechaCalendario_N3

Feature: Fecha_Calendario N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Fecha_Calendario caso N_tres
    When se debe insertar el dataset para las tablas de RAW para Fecha_Calendario caso N_tres
    And se debe insertar el dataset para las tablas de CUR para Fecha_Calendario caso N_tres
    Then se debe ejecutar el proceso de Fecha_Calendario caso N_tres

  Scenario: Validacion para tabla Fecha_Calendario "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del CUR para tabla Fecha_Calendario caso N_tres
    And hago una consulta a la tabla del MCP para tabla Fecha_Calendario caso N_tres
    Then valido que la insercion a la tabla CUR sea correcta para tabla Fecha_Calendario caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Fecha_Calendario caso N_tres

  Scenario: Validacion de logs Fecha_Calendario "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Fecha_Calendario caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Fecha_Calendario caso N_tres

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Fecha_Calendario caso N_tres