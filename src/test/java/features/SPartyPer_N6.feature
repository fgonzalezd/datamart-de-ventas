@sPartyPer_N6

Feature: S_PARTY_PER N6 - La ejecucion esta bloqueada

  Scenario: Ambientacion para S_PARTY_PER "La ejecucion esta bloqueada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para S_PARTY_PER caso N_seis
    When se debe insertar el dataset para las tablas de Oracle para S_PARTY_PER caso N_seis
    And se debe insertar el dataset para las tablas de hive para S_PARTY_PER caso N_seis
    And se crea el archivo lock en el UNIX para S_PARTY_PER caso N_seis
    Then se debe ejecutar el proceso de extraccion para S_PARTY_PER caso N_seis

  Scenario: Validacion para tabla S_PARTY_PER "La ejecucion esta bloqueada"
    When hago una consulta a la tabla del RAW para tabla S_PARTY_PER caso N_seis
    And hago una consulta a la tabla del MCP para tabla S_PARTY_PER caso N_seis
    Then valido que la insercion a la tabla RAW sea correcta para tabla S_PARTY_PER caso N_seis
    And valido que el registro de insercion en el MCP fue correcto para tabla S_PARTY_PER caso N_seis

  Scenario: Validacion de archivos de frecuencia S_PARTY_PER "La ejecucion esta bloqueada"
    When hago una consulta al archivo lock para S_PARTY_PER caso N_seis
    And hago una consulta al archivo de frecuencia para S_PARTY_PER caso N_seis
    Then valido que el archivo lock haya sido eliminado correctamente para S_PARTY_PER caso N_seis
    And valido que el archivo de secuencia tenga la ultima secuencia para S_PARTY_PER caso N_seis

  Scenario: Validacion de archivos en hdfs para S_PARTY_PER "La ejecucion esta bloqueada"
    When hago una consulta a los archivos en TO_IMPORT para S_PARTY_PER caso N_seis
    And hago una consulta a los archivos en TO_TRANSFER para S_PARTY_PER caso N_seis
    And hago una consulta a los archivos en LOADING para S_PARTY_PER caso N_seis
    Then valido que la cantidad de archivos en el hdfs sea correcta para S_PARTY_PER caso N_seis

  Scenario: Validacion de logs S_PARTY_PER "La ejecucion esta bloqueada"
    When hago una consulta a los logs generados en RAW para S_PARTY_PER caso N_seis
    Then valido que la cantidad de logs generados sea correcto para S_PARTY_PER caso N_seis

  Scenario: Limpieza ambientacion para S_PARTY_PER "La ejecucion esta bloqueada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para S_PARTY_PER caso N_seis