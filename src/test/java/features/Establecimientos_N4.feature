@establecimientos_N4

Feature: Establecimientos N4 - Metrica deshabilitada

  Scenario: Ambientacion para Establecimientos "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Establecimientos caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para Establecimientos caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para Establecimientos caso N_cuatro
    Then se debe ejecutar el proceso de extraccion para Establecimientos caso N_cuatro

  Scenario: Validacion para tabla Establecimientos "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla Establecimientos caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Establecimientos caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla Establecimientos caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Establecimientos caso N_cuatro

  Scenario: Validacion de archivos en hdfs para Establecimientos "Metrica deshabilitada"
    When hago una consulta a los archivos en TO_IMPORT para Establecimientos caso N_cuatro
    And hago una consulta a los archivos en TO_TRANSFER para Establecimientos caso N_cuatro
    And hago una consulta a los archivos en LOADING para Establecimientos caso N_cuatro
    Then valido que la cantidad de archivos en el hdfs sea correcta para Establecimientos caso N_cuatro

  Scenario: Validacion de logs Establecimientos "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Establecimientos caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Establecimientos caso N_cuatro

  Scenario: Limpieza ambientacion para Establecimientos "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Establecimientos caso N_cuatro