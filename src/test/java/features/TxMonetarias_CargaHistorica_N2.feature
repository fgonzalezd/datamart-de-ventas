@txMonetarias_CargaHistorica_N2

Feature: tx_Monetarias Carga Historica - Reprocesar un rango de fecha existente o nuevo del mismo mes

  Scenario: Ambientacion para tx_Monetarias Carga Historica "Reprocesar un rango de fecha existente o nuevo del mismo mes"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias Carga Historica N_dos
    When transfiero el archivo a una carpeta del servidor para tx_Monetarias Carga Historica N_dos
    And muevo el archivo a una ruta hdfs para tx_Monetarias Carga Historica N_dos
    And consulto el checksum del archivo transferido para tx_Monetarias Carga Historica N_dos
    Then valido el checksum de ambos archivos para tx_Monetarias Carga Historica N_dos
    And se ejecuta el proceso para tx_Monetarias Carga Historica N_dos

  Scenario: Validacion para tabla tx_Monetarias Carga Historica "Reprocesar un rango de fecha existente o nuevo del mismo mes"
    When hago una consulta a la tabla del RAW para tabla tx_Monetarias Carga Historica N_dos
    And hago una consulta a la tabla del MCP para tabla tx_Monetarias Carga Historica N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla tx_Monetarias Carga Historica N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla tx_Monetarias Carga Historica N_dos

  Scenario: Limpieza ambientacion para tx_Monetarias Carga Historica "Reprocesar un rango de fecha existente o nuevo del mismo mes"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias Carga Historica N_dos