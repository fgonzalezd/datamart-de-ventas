@ctssServicios_N8

Feature: Ctss_Servicios N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para Ctss_Servicios "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Ctss_Servicios caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Ctss_Servicios caso N_Ocho
    Then se debe ejecutar el proceso de extraccion y transferencia para Ctss_Servicios caso N_Ocho

  Scenario: Validacion de DRP Ctss_Servicios "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum del master Ctss_Servicios caso N_Ocho
    And hago una consulta al checksum del slave Ctss_Servicios caso N_Ocho
    Then valido que el archivo transferido este correcto Ctss_Servicios caso N_Ocho

  Scenario: Validacion de archivos en hdfs para Ctss_Servicios "Transferencia de multiples archivos DRP"
    When hago una consulta a los archivos en el hdfs del master para Ctss_Servicios caso N_Ocho
    And hago una consulta a los archivos en el hdfs del slave para Ctss_Servicios caso N_Ocho
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Ctss_Servicios caso N_Ocho
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Ctss_Servicios caso N_Ocho

  Scenario: Validacion de logs Ctss_Servicios "Transferencia de multiples archivos DRP"
    When hago una consulta a los logs generados del master para Ctss_Servicios caso N_Ocho
    And hago una consulta a los logs generados del slave para Ctss_Servicios caso N_Ocho
    Then valido que la cantidad de logs generados del master sea correcto para Ctss_Servicios caso N_Ocho
    And valido que la cantidad de logs generados del slave sea correcto para Ctss_Servicios caso N_Ocho


  Scenario: Limpieza ambientacion para Ctss_Servicios "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Ctss_Servicios caso N_Ocho