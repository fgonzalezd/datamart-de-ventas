package pages;

public class EnvironmentCtss_Page extends Base_Page{

    public static String environment = "src/test/resources/query/Environment_Ctss.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createCTSS_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("createCTSS_SERVICIOS");
        return create;
    }

    public static String createCTSS_TIPO_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("createCTSS_TIPO_SERVICIOS");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropCTSS_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("dropCTSS_SERVICIOS");
        return create;
    }

    public static String dropCTSS_TIPO_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("dropCTSS_TIPO_SERVICIOS");
        return create;
    }

    /**********************
     ***** DELETE RAW *****
     **********************/

    public static String deleteCTSS_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("deleteCTSS_SERVICIOS");
        return create;
    }

    public static String deleteCTSS_TIPO_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("deleteCTSS_TIPO_SERVICIOS");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL_CTSS_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_CTSS_SERVICIOS");
        return create;
    }

    public static String deletePROCESO_METRICA_REL_CTSS_TIPO_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_CTSS_TIPO_SERVICIOS");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }

    /**********************
     ***** DELETE LOG *****
     **********************/

    public static String logCTSS_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("logCTSS_SERVICIOS");
        return create;
    }

    public static String logCTSS_TIPO_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("logCTSS_TIPO_SERVICIOS");
        return create;
    }

    /***********************
     ***** DELETE HDFS *****
     ***********************/

    public static String hdfsCTSS_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("hdfsCTSS_SERVICIOS");
        return create;
    }

    public static String hdfsCTSS_TIPO_SERVICIOS() {
        String create = fetchQuery(environment).getProperty("hdfsCTSS_TIPO_SERVICIOS");
        return create;
    }
}
