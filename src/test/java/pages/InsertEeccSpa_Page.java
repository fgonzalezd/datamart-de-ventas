package pages;

public class InsertEeccSpa_Page extends Base_Page{

    public static String environment = "src/test/resources/insert/Insert_EeccSpa.xml";

    /*************************
     ***** INSERT ORIGEN *****
     *************************/

    public static String insert_ORIGEN() {
        String insert = fetchQuery(environment).getProperty("insert_ORIGEN");
        return insert;
    }

    /**********************
     ***** INSERT RAW *****
     **********************/

    public static String insert_RAW1() {
        String insert = fetchQuery(environment).getProperty("insert_RAW1");
        return insert;
    }

    public static String insert_RAW2() {
        String insert = fetchQuery(environment).getProperty("insert_RAW2");
        return insert;
    }

    /**********************
     ***** INSERT MCP *****
     **********************/

    public static String insertPROCESO_METRICA_REL_N1() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N1");
        return insert;
    }

    public static String insertEJECUCION_PROCESO1_N1() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO1_N1");
        return insert;
    }

    public static String insertEJECUCION_PROCESO2_N1() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO2_N1");
        return insert;
    }

    public static String insertEJECUCION_PROCESO3_N1() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO3_N1");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N2() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N2");
        return insert;
    }

    public static String insertEJECUCION_PROCESO1_N2() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO1_N2");
        return insert;
    }

    public static String insertEJECUCION_PROCESO2_N2() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO2_N2");
        return insert;
    }

    public static String insertEJECUCION_PROCESO3_N2() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO3_N2");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N3() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N3");
        return insert;
    }

    public static String insertEJECUCION_PROCESO1_N3() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO1_N3");
        return insert;
    }

    public static String insertEJECUCION_PROCESO2_N3() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO2_N3");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N4() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N4");
        return insert;
    }

    public static String insertEJECUCION_PROCESO1_N4() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO1_N4");
        return insert;
    }

    public static String insertEJECUCION_PROCESO2_N4() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO2_N4");
        return insert;
    }

    public static String insertEJECUCION_PROCESO3_N4() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO3_N4");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N5() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N5");
        return insert;
    }

    public static String insertEJECUCION_PROCESO1_N5() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO1_N5");
        return insert;
    }

    public static String insertEJECUCION_PROCESO2_N5() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO2_N5");
        return insert;
    }

    public static String insertEJECUCION_PROCESO3_N5() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO3_N5");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N6() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N6");
        return insert;
    }

    public static String insertEJECUCION_PROCESO1_N6() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO1_N6");
        return insert;
    }

    public static String insertEJECUCION_PROCESO2_N6() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO2_N6");
        return insert;
    }

    public static String insertEJECUCION_PROCESO3_N6() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO3_N6");
        return insert;
    }
}
