@parcat_Credito_N7

Feature: parcat_Credito N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para parcat_Credito caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para parcat_Credito caso N_Siete
    Then se debe ejecutar el proceso de extraccion con una fecha del 10/7/2019 para parcat_Credito caso N_Siete

  Scenario: Validacion de DRP parcat_Credito "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del archivo origen parcat_Credito caso N_Siete
    And hago una consulta al checksum del archivo transferido parcat_Credito caso N_Siete
    Then valido que el archivo transferido este correcto parcat_Credito caso N_Siete

  Scenario: Limpieza ambientacion para "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para parcat_Credito caso N_Siete