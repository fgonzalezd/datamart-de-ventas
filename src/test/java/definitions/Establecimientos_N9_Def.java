package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.Base_Page;
import pages.EnvironmentArc_Page;
import pages.InsertEstablecimientos_Page;
import pages.QueryTableArc_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class Establecimientos_N9_Def {

    public static PropertiesInit prop;
    public static Base_Page page;
    public static EnvironmentArc_Page environment;
    public ArrayList<String> logs = new ArrayList<>();

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Establecimientos caso N_Nueve$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Establecimientos_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.logESTABLECIMIENTOS());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsESTABLECIMIENTOS());
    }

    @When("^Se ejecuta el proceso de extraccion con una fecha erronea para Establecimientos caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_extraccion_con_una_fecha_erronea_para_Establecimientos_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 50 0190709 ARC N");
    }

    @Then("^se valida que el proceso de extraccion se haya terminado con error Establecimientos caso N_Nueve$")
    public void se_valida_que_el_proceso_de_extraccion_se_haya_terminado_con_error_Establecimientos_caso_N_Nueve() throws Throwable {
        
    }

    @When("^Se ejecuta el proceso de carga STG con una fecha erronea para Establecimientos caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_STG_con_una_fecha_erronea_para_Establecimientos_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 51 S01907j9 ARC N");
    }

    @Then("^se valida que el proceso de carga STG se haya terminado con error Establecimientos caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_STG_se_haya_terminado_con_error_Establecimientos_caso_N_Nueve() throws Throwable {
        
    }

    @When("^Se ejecuta el proceso de carga RAW con una fecha erronea para Establecimientos caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_RAW_con_una_fecha_erronea_para_Establecimientos_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 53 2019:07:09 ARC N");
    }

    @Then("^se valida que el proceso de carga RAW se haya terminado con error Establecimientos caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_RAW_se_haya_terminado_con_error_Establecimientos_caso_N_Nueve() throws Throwable {
        
    }

    @When("^hago una consulta a los logs generados en RAW para Establecimientos caso N_Nueve$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Establecimientos_caso_N_Nueve() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/arc/establecimientos -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            String find = "";
            while ((find = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + find);
                logs.add(find);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Establecimientos caso N_Nueve$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Establecimientos_caso_N_Nueve() throws Throwable {
        assertEquals(logs, logs);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logs + " de 3");
    }
}
