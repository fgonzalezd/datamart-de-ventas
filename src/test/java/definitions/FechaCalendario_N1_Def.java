package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentTabGenerales_Page;
import pages.InsertFechaCalendario_Page;
import pages.QueryTableTabGenerales_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class FechaCalendario_N1_Def {

    public static PropertiesInit prop;
    public static EnvironmentTabGenerales_Page environment;
    public static InsertFechaCalendario_Page page;
    public static QueryTableTabGenerales_Page query;
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvMCP = "src/test/resources/dataset/FechaCalendario/fechaCalendarioMCP_N1.csv";
    public String destinoCUR;
    public String destinoFestivo;
    public ArrayList<String[]> destinoMCP = new ArrayList<>();
    public ArrayList<String[]> datasetMCP = new ArrayList<>();
    public String logs;

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Fecha_Calendario caso N_uno$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Fecha_Calendario_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTab(), environment.deleteDIAS_FESTIVOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTiempo(), environment.deleteFECHA_CALENDARIO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserCUR(), prop.PassCUR(), "rm -R /data/environment/hdp_log/load_cur/tabgeneral_tiempo/fecha_calendario");
    }

    @When("^se debe insertar el dataset para las tablas de RAW para Fecha_Calendario caso N_uno$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_RAW_para_Fecha_Calendario_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTab(), page.insert_DIAS_FESTIVOS09());
    }

    @When("^se debe insertar el dataset para las tablas de CUR para Fecha_Calendario caso N_uno$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_CUR_para_Fecha_Calendario_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N1());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO_N1());
    }

    @Then("^se debe ejecutar el proceso de Fecha_Calendario caso N_uno$")
    public void se_debe_ejecutar_el_proceso_de_Fecha_Calendario_caso_N_uno() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserCUR(), prop.PassCUR(),
                "sh -x /data/environment/hdp_app/load_cur/tabgeneral_tiempo/shl/Shl_carga_cur_fecha_calendario.sh 48 20190709 TABGENERAL_TIEMPO N");
    }

    @When("^hago una consulta a la tabla del CUR para tabla Fecha_Calendario caso N_uno$")
    public void hago_una_consulta_a_la_tabla_del_CUR_para_tabla_Fecha_Calendario_caso_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserCUR(), prop.PassCUR());
            connHive.setSchema(prop.HiveSchemaCurTiempo());
            stmtHive = connHive.createStatement();

            ResultSet resCount = stmtHive.executeQuery(query.queryFECHA_CALENDARIO());
            while (resCount.next()) {
                destinoCUR = resCount.getString(1);
            }

            ResultSet resFestivo = stmtHive.executeQuery(query.queryFECHA_CALENDARIO_FESTIVO());
            while (resFestivo.next()) {
                destinoFestivo = resFestivo.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("^hago una consulta a la tabla del MCP para tabla Fecha_Calendario caso N_uno$")
    public void hago_una_consulta_a_la_tabla_del_MCP_para_tabla_Fecha_Calendario_caso_N_uno() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserCUR(), prop.PassCUR());
            connHive.setSchema(prop.HiveSchemaMCP());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_FECHA_CALENDARIO());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoMCP.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvMCP))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetMCP.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido que la insercion a la tabla CUR sea correcta para tabla Fecha_Calendario caso N_uno$")
    public void valido_que_la_insercion_a_la_tabla_CUR_sea_correcta_para_tabla_Fecha_Calendario_caso_N_uno() throws Throwable {
        Reporter.addStepLog("Consulta Dias = " + destinoCUR);
        Reporter.addStepLog("Esperado Dias = 365");
        System.out.println("Destino " + destinoCUR);
//      assertEquals("365", destinoCUR);

        Reporter.addStepLog("------------------------------------------------------------------------------");
        System.out.println(" ");

        Reporter.addStepLog("Consulta Dias Festivos = " + destinoFestivo);
        Reporter.addStepLog("Esperado Dias Festivos = 5");
        System.out.println("Destino Festivos " + destinoFestivo);
//      assertEquals("5", destinoFestivo);
    }

    @Then("^valido que el registro de insercion en el MCP fue correcto para tabla Fecha_Calendario caso N_uno$")
    public void valido_que_el_registro_de_insercion_en_el_MCP_fue_correcto_para_tabla_Fecha_Calendario_caso_N_uno() throws Throwable {
        //Cantidad de registros
        ResultSet resRows = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_COUNT_FECHA_CALENDARIO());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }
        //Validacion de datos
        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            System.out.println("Dataset MCP " + i + " = " + Arrays.deepToString(datasetMCP.get(i)));
//            assertTrue(Arrays.deepToString(destinoMCP.get(i)).equals(Arrays.deepToString(destinoMCP.get(i))));
        }
        System.out.println(" ");
        destinoMCP.clear();
        datasetMCP.clear();
    }

    @When("^hago una consulta a los logs generados en RAW para Fecha_Calendario caso N_uno$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Fecha_Calendario_caso_N_uno() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.UserCUR(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.PassCUR());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("find /data/environment/hdp_log/load_cur/tabgeneral_tiempo/fecha_calendario -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((logs = reader.readLine()) != null) {
                System.out.println(++index + " : " + logs);
                Reporter.addStepLog(logs);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Fecha_Calendario caso N_uno$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Fecha_Calendario_caso_N_uno() throws Throwable {
//        assertEquals("1", logs);
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Fecha_Calendario caso N_uno$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Fecha_Calendario_caso_N_uno() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawTab(), environment.deleteDIAS_FESTIVOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTiempo(), environment.deleteFECHA_CALENDARIO());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
    }
}
