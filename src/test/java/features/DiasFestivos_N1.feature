@diasFestivos_N1

Feature: dias_Festivos N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para dias_Festivos caso N_uno
    When se debe insertar el dataset para las tablas de Oracle para dias_Festivos caso N_uno
    And se debe insertar el dataset para las tablas de hive para dias_Festivos caso N_uno
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para dias_Festivos caso N_uno

  Scenario: Validacion para tabla dias_Festivos "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del RAW para tabla dias_Festivos caso N_uno
    And hago una consulta a la tabla del MCP para tabla dias_Festivos caso N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla dias_Festivos caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla dias_Festivos caso N_uno

  Scenario: Validacion de logs dias_Festivos "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los logs generados en RAW para dias_Festivos caso N_uno
    Then valido que la cantidad de logs generados sea correcto para dias_Festivos caso N_uno

  Scenario: Limpieza ambientacion para "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para dias_Festivos caso N_uno