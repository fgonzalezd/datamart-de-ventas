package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentCtss_Page;
import pages.InsertCtssServicios_Page;
import pages.QueryTableCtss_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class CtssServicios_N5_Def {

    public static PropertiesInit prop;
    public static EnvironmentCtss_Page environment;
    public static QueryTableCtss_Page query;
    public static InsertCtssServicios_Page page;
    public Connection connHive;
    public Statement stmtHive;
    public int rows;
    public static String csvRAW = "src/test/resources/dataset/CtssServicios/ctssServiciosRAW_N5.csv";
    public static String csvMCP = "src/test/resources/dataset/CtssServicios/ctssServiciosMCP_N5.csv";
    public ArrayList<String[]> destinoRAW = new ArrayList<>();
    public ArrayList<String[]> datasetRAW = new ArrayList<>();
    public ArrayList<String[]> destinoMCP = new ArrayList<>();
    public ArrayList<String[]> datasetMCP = new ArrayList<>();
    public ArrayList<String> logs = new ArrayList<>();

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Ctss_Servicios caso N_cinco$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropCTSS_SERVICIOS());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createCTSS_SERVICIOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawCtss(), environment.deleteCTSS_SERVICIOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL_CTSS_SERVICIOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.logCTSS_SERVICIOS());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsCTSS_SERVICIOS());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para Ctss_Servicios caso N_cinco$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN());
    }

    @When("^se debe insertar el dataset para las tablas de hive para Ctss_Servicios caso N_cinco$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_hive_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawCtss(), page.insert_RAW1());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawCtss(), page.insert_RAW2());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertPROCESO_METRICA_REL_N5());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO1_N5());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO2_N5());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), page.insertEJECUCION_PROCESO3_N5());
    }

    @Then("^se debe ejecutar el proceso de extraccion para Ctss_Servicios caso N_cinco$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 66 20190709 CTSS N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 67 20190709 CTSS N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 69 20190709 CTSS N");
    }

    @When("^hago una consulta a la tabla del RAW para tabla Ctss_Servicios caso N_cinco$")
    public void hago_una_consulta_a_la_tabla_del_RAW_para_tabla_Ctss_Servicios_caso_N_cinco() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaRawCtss());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryCTSS_SERVICIOS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoRAW.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvRAW))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetRAW.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("^hago una consulta a la tabla del MCP para tabla Ctss_Servicios caso N_cinco$")
    public void hago_una_consulta_a_la_tabla_del_MCP_para_tabla_Ctss_Servicios_caso_N_cinco() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserRAW(), prop.PassRAW());
            connHive.setSchema(prop.HiveSchemaMCP());
            stmtHive = connHive.createStatement();

            ResultSet resHive = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_CTSS_SERVICIOS());
            int columnCount = resHive.getMetaData().getColumnCount();
            while (resHive.next()) {
                String[] row = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    row[i] = resHive.getString(i + 1);
                }
                destinoMCP.add(row);
            }

            try (BufferedReader br = new BufferedReader(new FileReader(csvMCP))) {
                String line = "";
                while ((line = br.readLine()) != null) {
                    datasetMCP.add(line.split(","));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido que la insercion a la tabla RAW sea correcta para tabla Ctss_Servicios caso N_cinco$")
    public void valido_que_la_insercion_a_la_tabla_RAW_sea_correcta_para_tabla_Ctss_Servicios_caso_N_cinco() throws Throwable {
        ResultSet resRows = stmtHive.executeQuery(query.queryCOUNT_CTSS_SERVICIOS());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }

        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoRAW.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino RAW " + i + " = " + Arrays.deepToString(destinoRAW.get(i)));
            System.out.println("Dataset RAW " + i + " = " + Arrays.deepToString(destinoRAW.get(i)));
            assertEquals(Arrays.deepToString(destinoRAW.get(i)), Arrays.deepToString(destinoRAW.get(i)));
        }
        System.out.println(" ");
        destinoRAW.clear();
        datasetRAW.clear();
    }

    @Then("^valido que el registro de insercion en el MCP fue correcto para tabla Ctss_Servicios caso N_cinco$")
    public void valido_que_el_registro_de_insercion_en_el_MCP_fue_correcto_para_tabla_Ctss_Servicios_caso_N_cinco() throws Throwable {
        ResultSet resRows = stmtHive.executeQuery(query.queryEJECUCION_PROCESO_COUNT_CTSS_SERVICIOS());
        while (resRows.next()) {
            rows = resRows.getInt(1);
        }

        for (int i = 0; i < rows; i++) {
            Reporter.addStepLog("Consulta = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("Esperado = " + Arrays.deepToString(destinoMCP.get(i)));
            Reporter.addStepLog("------------------------------------------------------------------------------");
            System.out.println("Destino MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            System.out.println("Dataset MCP " + i + " = " + Arrays.deepToString(destinoMCP.get(i)));
            assertEquals(Arrays.deepToString(destinoMCP.get(i)), Arrays.deepToString(destinoMCP.get(i)));
        }
        System.out.println(" ");
        destinoMCP.clear();
        datasetMCP.clear();
    }

    @When("^hago una consulta a los archivos en TO_IMPORT para Ctss_Servicios caso N_cinco$")
    public void hago_una_consulta_a_los_archivos_en_TO_IMPORT_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        
    }

    @When("^hago una consulta a los archivos en TO_TRANSFER para Ctss_Servicios caso N_cinco$")
    public void hago_una_consulta_a_los_archivos_en_TO_TRANSFER_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        
    }

    @When("^hago una consulta a los archivos en LOADING para Ctss_Servicios caso N_cinco$")
    public void hago_una_consulta_a_los_archivos_en_LOADING_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        
    }

    @Then("^valido que la cantidad de archivos en el hdfs sea correcta para Ctss_Servicios caso N_cinco$")
    public void valido_que_la_cantidad_de_archivos_en_el_hdfs_sea_correcta_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        
    }

    @When("^hago una consulta a los logs generados en RAW para Ctss_Servicios caso N_cinco$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/ctss/ctss_servicios -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String find = "";
            while ((find = reader.readLine()) != null) {
                System.out.println("CANTIDAD DE LOGS = " + find);
                logs.add(find);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Ctss_Servicios caso N_cinco$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        assertEquals(logs, logs);
        Reporter.addStepLog("La cantidad de logs encontrados son " + logs + " de 3");
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Ctss_Servicios caso N_cinco$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Ctss_Servicios_caso_N_cinco() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropCTSS_SERVICIOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaRawCtss(), environment.deleteCTSS_SERVICIOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deletePROCESO_METRICA_REL_CTSS_SERVICIOS());
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserRAW(), prop.PassRAW(), prop.HiveSchemaMCP(), environment.deleteEJECUCION_PROCESO());
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.UserRAW(), prop.PassRAW(), environment.hdfsCTSS_SERVICIOS());
    }
}
