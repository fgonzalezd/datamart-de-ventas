@cobroComision_N8

Feature: Cobro_Comision_Retencion N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Cobro_Comision_Retencion caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Cobro_Comision_Retencion caso N_Ocho
    Then se debe ejecutar el proceso de extraccion para Cobro_Comision_Retencion caso N_Ocho

  Scenario: Validacion de DRP Cobro_Comision_Retencion "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum de los archivos de origen Cobro_Comision_Retencion caso N_Ocho
    And hago una consulta al checksum de los archivos transferidos Cobro_Comision_Retencion caso N_Ocho
    Then valido que los archivos transferidos esten correcto Cobro_Comision_Retencion caso N_Ocho

  Scenario: Validacion de logs Cobro_Comision_Retencion "Transferencia de multiples archivos DRP"
    When hago una consulta a los logs generados en RAW para Cobro_Comision_Retencion caso N_Ocho
    Then valido que la cantidad de logs generados sea correcto para Cobro_Comision_Retencion caso N_Ocho

  Scenario: Limpieza ambientacion para "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Cobro_Comision_Retencion caso N_Ocho