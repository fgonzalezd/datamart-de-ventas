@tipoCambio_N2

Feature: Tipo_Cambio N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Tipo_Cambio caso N_dos
    When se debe insertar el dataset para las tablas de Oracle para Tipo_Cambio caso N_dos
    And se debe insertar el dataset para las tablas de hive para Tipo_Cambio caso N_dos
    Then se debe ejecutar el proceso de extraccion con una fecha del 8/7/2019 para Tipo_Cambio caso N_dos

  Scenario: Validacion para tabla Tipo_Cambio "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del RAW para tabla Tipo_Cambio caso N_dos
    And hago una consulta a la tabla del MCP para tabla Tipo_Cambio caso N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla Tipo_Cambio caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla Tipo_Cambio caso N_dos

  Scenario: Validacion de logs Tipo_Cambio "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los logs generados en RAW para Tipo_Cambio caso N_dos
    Then valido que la cantidad de logs generados sea correcto para Tipo_Cambio caso N_dos

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Tipo_Cambio caso N_dos