package pages;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import org.dbunit.database.*;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.csv.CsvDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlWriter;
import org.dbunit.operation.DatabaseOperation;

import java.io.*;
import java.sql.*;
import java.util.Calendar;
import java.util.Properties;

public class Base_Page {

    /**
     * Metodo para insertar datos desde un XML con DBUNIT
     */

    public static void insertDataXML(String driverName,
                                     String urlDB,
                                     String userDB,
                                     String passworDB,
                                     String schemaBD,
                                     String nameXML) throws SQLException {

        Connection conn = null;
        try {
            DriverManager.registerDriver((Driver) Class.forName(driverName).newInstance());
            conn = DriverManager.getConnection(urlDB, userDB, passworDB);
            DatabaseConnection connection = new DatabaseConnection(conn, schemaBD);
            connection.getConfig().setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);

            DatabaseOperation.INSERT.execute(connection, new FlatXmlDataSet(new FileInputStream("src/test/resources/dataset/" + nameXML + ".xml")));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * Metodo para insertar datos desde un CSV con DBUNIT
     */

    public static void insertDataCSV(String driverName,
                                     String urlDB,
                                     String userDB,
                                     String passworDB,
                                     String schemaBD) throws SQLException {

        Connection conn = null;
        try {
            DriverManager.registerDriver((Driver) Class.forName(driverName).newInstance());
            conn = DriverManager.getConnection(urlDB, userDB, passworDB);
            IDatabaseConnection connection = new DatabaseConnection(conn, schemaBD);
            connection.getConfig().setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);

            IDataSet dataset = new CsvDataSet(new File("./src/test/resources/dataset"));
            DatabaseOperation.INSERT.execute(connection, dataset);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * Metodo para borrar datos cargados desde un XML con DBUNIT
     */

    public static void deleteDataXML(String driverName,
                                     String urlDB,
                                     String userDB,
                                     String passworDB,
                                     String schemaBD,
                                     String nameXML) throws SQLException {
        Connection conn = null;
        try {
            DriverManager.registerDriver((Driver) Class.forName(driverName).newInstance());
            conn = DriverManager.getConnection(urlDB, userDB, passworDB);
            IDatabaseConnection connection = new DatabaseConnection(conn, schemaBD);
            connection.getConfig().setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);

            DatabaseOperation.DELETE.execute(connection, new FlatXmlDataSet(new FileInputStream("src/test/resources/dataset/" + nameXML + ".xml")));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * Metodo para borrar datos cargados desde un CSV con DBUNIT
     */

    public static void deleteDataCSV(String driverName,
                                     String urlDB,
                                     String userDB,
                                     String passworDB,
                                     String schemaBD) throws SQLException {
        Connection conn = null;
        try {
            DriverManager.registerDriver((Driver) Class.forName(driverName).newInstance());
            conn = DriverManager.getConnection(urlDB, userDB, passworDB);
            IDatabaseConnection connection = new DatabaseConnection(conn, schemaBD);
            connection.getConfig().setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);

            IDataSet dataset = new CsvDataSet(new File("./src/test/resources/dataset"));
            DatabaseOperation.DELETE.execute(connection, dataset);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * Metodo que extrae los datos de una tabla en formato XML con DBUNIT
     */

    public static void generateXML(String driverName,
                                   String urlDB,
                                   String userDB,
                                   String passwordDB,
                                   String schemaBD,
                                   String nameXML) throws SQLException {

        Connection conn = null;

        try {
            DriverManager.registerDriver((Driver) Class.forName(driverName).newInstance());
            conn = DriverManager.getConnection(urlDB, userDB, passwordDB);
            IDatabaseConnection connection = new DatabaseConnection(conn, schemaBD);
            connection.getConfig().setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);
            connection.getConfig().setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);

            DatabaseSequenceFilter filter = new DatabaseSequenceFilter(connection);
            IDataSet datasetAll = new FilteredDataSet(filter, connection.createDataSet());
            QueryDataSet partialDataSet = new QueryDataSet(connection);

            String[] listTableNames = filter.getTableNames(datasetAll);
            for (int i = 0; i < listTableNames.length; i++) {
                final String tableName = listTableNames[i];
                partialDataSet.addTable(tableName);
            }

            FlatXmlWriter datasetWriter = new FlatXmlWriter(new FileOutputStream("src/test/resources/dataset/" + nameXML + ".xml"));

            datasetWriter.write(partialDataSet);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    /**
     * Metodo para conectarse por SSH, imprime el log en consola y reporte
     */

    public static void sshConnector(String host,
                                    int port,
                                    String username,
                                    String password,
                                    String command) throws Exception {

        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(username, host, port);
            sessionSSH.setPassword(password);

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand(command);
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            int index = 0;
            while ((line = reader.readLine()) != null) {
                System.out.println(++index + " : " + line);
                Reporter.addStepLog(line);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    /**
     * Metodo para conectarse por SSH, imprime el log en consola y reporte
     */

    public static void sshConnectorChannel(String host,
                                           int port,
                                           String username,
                                           String password,
                                           String ssh1,
                                           String ssh2) throws Exception {

        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        Channel channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(username, host, port);
            sessionSSH.setPassword(password);

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = sessionSSH.openChannel("shell");
            OutputStream ops = channelSSH.getOutputStream();
            PrintStream ps = new PrintStream(ops, true);

            // executar comando
            channelSSH.connect();
            InputStream input = channelSSH.getInputStream();
            ps.println(ssh1);
            ps.println(ssh2);
            ps.println("exit");
            ps.close();

            printResult(input, channelSSH);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    /**
     * @param input
     * @param channel
     */
    private static void printResult(InputStream input,
                                    Channel channel) throws Exception {
        int SIZE = 1024;
        byte[] tmp = new byte[SIZE];
        while (true) {
            while (input.available() > 0) {
                int i = input.read(tmp, 0, SIZE);
                if (i < 0)
                    break;
                System.out.print(new String(tmp, 0, i));
            }
            if (channel.isClosed()) {
                System.out.println("exit-status: " + channel.getExitStatus());
                break;
            }
            try {
                Thread.sleep(300);
            } catch (Exception ee) {
            }
        }
    }

    /**
     * Metodo de manejo para el JDBC
     */

    public static void jdbcConnector(String driverName,
                                     String urlDB,
                                     String userDB,
                                     String passwordDB,
                                     String schemaBD,
                                     String query) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName(driverName);
            conn = DriverManager.getConnection(urlDB, userDB, passwordDB);
            conn.setSchema(schemaBD);
            stmt = conn.createStatement();
            stmt.execute(query);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo que entrega la fecha diaria
     */

    public static String datesh() {
        Calendar ahora = Calendar.getInstance();
        Integer n = new Integer((ahora.get(Calendar.YEAR) * 10000) + ((ahora.get(Calendar.MONTH) + 1) * 100) + (ahora.get(Calendar.DAY_OF_MONTH)));
        return n.toString();
    }

    /**
     * Metodo que entrega querys desde un XML
     */

    public static Properties fetchQuery(String queryPath) {
        Properties properties = new Properties();
        try {
            FileInputStream fileInputStream = new FileInputStream(queryPath);
            properties.loadFromXML(fileInputStream);
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

}